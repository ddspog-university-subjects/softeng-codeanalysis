package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_21 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_21.test1");


    junit.framework.AssertionFailedError var2 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var6 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var6);
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var6);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var9 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var2, "", (java.lang.Object[])var6);
    org.junit.Assert.assertNull("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", (java.lang.Object)var2);

  }

}
