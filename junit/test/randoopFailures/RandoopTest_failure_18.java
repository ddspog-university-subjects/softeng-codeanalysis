package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_18 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_18.test1");


    byte[] var0 = new byte[] { };
    byte[] var3 = new byte[] { (byte)0, (byte)(-1)};
    org.junit.Assert.assertArrayEquals(var0, var3);

  }

}
