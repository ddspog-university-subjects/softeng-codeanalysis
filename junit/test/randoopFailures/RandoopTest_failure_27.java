package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_27 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_27.test1");


    long[] var2 = new long[] { 100L};
    long[] var5 = new long[] { 100L, (-1L)};
    org.junit.Assert.assertArrayEquals("{} expected:<[org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@94e474> but was:<100>]> but was:<[]>", var2, var5);

  }

}
