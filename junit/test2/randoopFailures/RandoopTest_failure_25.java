package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_25 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_25.test1");


    double[] var3 = new double[] { 10.0d, 0.0d};
    double[] var7 = new double[] { 10.0d, 1.0d, 0.0d};
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", var3, var7, (-1.0d));

  }

}
