package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_20 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_20.test1");


    boolean[] var2 = new boolean[] { false};
    boolean[] var5 = new boolean[] { true, true};
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>", var2, var5);

  }

}
