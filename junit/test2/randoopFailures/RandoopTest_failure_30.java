package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_30 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_30.test1");


    boolean[] var1 = new boolean[] { false};
    boolean[] var5 = new boolean[] { false, true, false};
    org.junit.Assert.assertArrayEquals(var1, var5);

  }

}
