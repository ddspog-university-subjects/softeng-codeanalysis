package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_28 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_28.test1");


    short[] var4 = new short[] { (short)0, (short)0, (short)0};
    short[] var6 = new short[] { (short)100};
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>", var4, var6);

  }

}
