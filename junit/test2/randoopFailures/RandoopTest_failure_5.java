package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_5 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_5.test1");


    short[] var2 = new short[] { (short)10, (short)(-1)};
    short[] var3 = new short[] { };
    org.junit.Assert.assertArrayEquals(var2, var3);

  }

}
