package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_7 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_7.test1");


    char[] var3 = new char[] { ' ', ' '};
    char[] var4 = new char[] { };
    org.junit.Assert.assertArrayEquals("hi!", var3, var4);

  }

}
