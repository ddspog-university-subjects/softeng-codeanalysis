package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_14 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_14.test1");


    int[] var2 = new int[] { 1, 10};
    int[] var6 = new int[] { 10, 1, 1};
    org.junit.Assert.assertArrayEquals(var2, var6);

  }

}
