package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_17 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_17.test1");


    junit.framework.TestResult var1 = new junit.framework.TestResult();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.TestSuite var7 = new junit.framework.TestSuite(var4);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    java.lang.String var17 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var16);
    var1.addError((junit.framework.Test)var7, (java.lang.Throwable)var16);
    org.junit.runners.model.InitializationError var19 = new org.junit.runners.model.InitializationError((java.lang.Throwable)var16);
    org.junit.Assert.assertEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object)var16, (java.lang.Object)(short)100);

  }

}
