package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_10 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_10.test1");


    java.lang.Class[] var2 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var3 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var2);
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var2);
    junit.framework.TestResult var5 = new junit.framework.TestResult();
    org.junit.Assert.assertEquals((java.lang.Object)var2, (java.lang.Object)var5);

  }

}
