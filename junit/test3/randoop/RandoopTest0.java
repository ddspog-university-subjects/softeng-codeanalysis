package randoop;

import junit.framework.*;

public class RandoopTest0 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test1");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      java.lang.Class var1 = org.junit.internal.Classes.getClass("hi!");
      fail("Expected exception of type java.lang.ClassNotFoundException");
    } catch (java.lang.ClassNotFoundException e) {
      // Expected exception.
    }

  }

  public void test2() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test2");


    junit.framework.Assert.assertFalse(false);

  }

  public void test3() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test3");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("hi!", 100, (-1));
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test4() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test4");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals('4', '#');
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test5() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test5");


    org.junit.Assert.assertNotEquals("", (-1.0d), 1.0d, 1.0d);

  }

  public void test6() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test6");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("", 10.0f, 1.0f, 1.0f);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test7() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test7");


    junit.framework.Assert.assertNotNull((java.lang.Object)true);

  }

  public void test8() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test8");


    org.junit.experimental.max.MaxCore var1 = org.junit.experimental.max.MaxCore.forFolder("");
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);

  }

  public void test9() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test9");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.runners.MethodSorters var1 = org.junit.runners.MethodSorters.valueOf("hi!");
      fail("Expected exception of type java.lang.IllegalArgumentException");
    } catch (java.lang.IllegalArgumentException e) {
      // Expected exception.
    }

  }

  public void test10() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test10");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals((-1.0f), 10.0f, (-1.0f));
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test11() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test11");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals((java.lang.Object)10, (java.lang.Object)(-1.0d));
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test12() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test12");


    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    org.junit.Assert.assertNotEquals((java.lang.Object)10, (java.lang.Object)true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);

  }

  public void test13() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test13");


    org.junit.Assume.assumeFalse(false);

  }

  public void test14() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test14");


    junit.framework.Assert.assertEquals("", (short)0, (short)0);

  }

  public void test15() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test15");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals((byte)10, (byte)0);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test16() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test16");


    char[] var3 = new char[] { '#', '#', '4'};
    char[] var7 = new char[] { '4', '4', 'a'};
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.Assert.assertArrayEquals(var3, var7);
      fail("Expected exception of type org.junit.internal.ArrayComparisonFailure");
    } catch (org.junit.internal.ArrayComparisonFailure e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);

  }

  public void test17() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test17");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals((-1), 1);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test18() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test18");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals(100.0d, 10.0d, 0.0d);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test19() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test19");


    org.junit.internal.runners.statements.FailOnTimeout.Builder var0 = org.junit.internal.runners.statements.FailOnTimeout.builder();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);

  }

  public void test20() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test20");


    org.junit.Assume.assumeFalse("{}", false);

  }

  public void test21() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test21");


    org.junit.internal.builders.SuiteMethodBuilder var1 = new org.junit.internal.builders.SuiteMethodBuilder();
    org.junit.Assert.assertNotEquals((java.lang.Object)1.0d, (java.lang.Object)var1);

  }

  public void test22() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test22");


    org.junit.validator.PublicClassValidator var0 = new org.junit.validator.PublicClassValidator();

  }

  public void test23() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test23");


    int[] var1 = new int[] { 0};
    int[] var3 = new int[] { 100};
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.Assert.assertArrayEquals(var1, var3);
      fail("Expected exception of type org.junit.internal.ArrayComparisonFailure");
    } catch (org.junit.internal.ArrayComparisonFailure e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);

  }

  public void test24() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test24");


    org.junit.Assert.assertNotNull((java.lang.Object)10);

  }

  public void test25() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test25");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.fail();
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test26() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test26");


    org.junit.rules.Stopwatch var0 = new org.junit.rules.Stopwatch();

  }

  public void test27() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test27");


    java.lang.String var1 = junit.runner.BaseTestRunner.getPreference("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);

  }

  public void test28() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test28");


    org.junit.experimental.theories.suppliers.TestedOnSupplier var0 = new org.junit.experimental.theories.suppliers.TestedOnSupplier();

  }

  public void test29() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test29");


    org.junit.Assert.assertNotEquals(1.0f, 0.0f, (-1.0f));

  }

  public void test30() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test30");


    junit.framework.Assert.assertTrue(true);

  }

  public void test31() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test31");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    org.junit.runner.Result var6 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var7 = var6.createListener();
    boolean var8 = var0.equals((java.lang.Object)var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);

  }

  public void test32() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test32");


    org.junit.Assert.assertEquals(10.0f, 100.0f, 100.0f);

  }

  public void test33() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test33");


    java.lang.annotation.Annotation[] var1 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var2 = org.junit.runner.Description.createSuiteDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", var1);
    boolean var3 = var2.isEmpty();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);

  }

  public void test34() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test34");


    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var5);
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var5);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var5);
    org.junit.experimental.categories.Categories.CategoryFilter var9 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var5);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("hi!", (java.lang.Object)(-1.0d), (java.lang.Object)var5);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);

  }

  public void test35() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test35");


    org.junit.Assert.assertNotEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", 10L, 0L);

  }

  public void test36() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test36");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals((-1), 10);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test37() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test37");


    junit.framework.AssertionFailedError var1 = new junit.framework.AssertionFailedError("hi!");
    org.junit.runners.model.InitializationError var2 = new org.junit.runners.model.InitializationError((java.lang.Throwable)var1);

  }

  public void test38() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test38");


    java.lang.annotation.Annotation[] var2 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var3 = org.junit.runner.Description.createSuiteDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", var2);
    org.junit.runner.Result var4 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var5 = var4.createListener();
    int var6 = var4.getIgnoreCount();
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.failNotEquals("hi!", (java.lang.Object)var2, (java.lang.Object)var6);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == 0);

  }

  public void test39() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test39");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    boolean var1 = var0.isEmpty();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var1 == true);

  }

  public void test40() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test40");


    org.junit.rules.TemporaryFolder.Builder var0 = org.junit.rules.TemporaryFolder.builder();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);

  }

  public void test41() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test41");


    org.junit.Assert.assertEquals(0.0d, 0.0d, 100.0d);

  }

  public void test42() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test42");


    java.lang.annotation.Annotation[] var4 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var5 = org.junit.runner.Description.createTestDescription("hi!", "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", var4);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.runner.Description var6 = org.junit.runner.Description.createSuiteDescription("", (java.io.Serializable)1.0d, var4);
      fail("Expected exception of type java.lang.IllegalArgumentException");
    } catch (java.lang.IllegalArgumentException e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);

  }

  public void test43() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test43");


    org.junit.runners.parameterized.BlockJUnit4ClassRunnerWithParametersFactory var0 = new org.junit.runners.parameterized.BlockJUnit4ClassRunnerWithParametersFactory();

  }

  public void test44() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test44");


    junit.extensions.ActiveTestSuite var0 = new junit.extensions.ActiveTestSuite();
    junit.extensions.ActiveTestSuite var1 = new junit.extensions.ActiveTestSuite();
    junit.framework.TestResult var2 = new junit.framework.TestResult();
    junit.framework.TestResult var3 = new junit.framework.TestResult();
    java.lang.Class[] var6 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var6);
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var6);
    junit.framework.TestSuite var9 = new junit.framework.TestSuite(var6);
    junit.framework.AssertionFailedError var11 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var15 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var15);
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var15);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var18 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var11, "", (java.lang.Object[])var15);
    java.lang.String var19 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var18);
    var3.addError((junit.framework.Test)var9, (java.lang.Throwable)var18);
    java.lang.String var21 = var9.getName();
    var2.endTest((junit.framework.Test)var9);
    java.util.Enumeration var23 = var2.errors();
    var0.runTest((junit.framework.Test)var1, var2);
    var1.runFinished();
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Test var27 = var1.testAt((-1));
      fail("Expected exception of type java.lang.ArrayIndexOutOfBoundsException");
    } catch (java.lang.ArrayIndexOutOfBoundsException e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var19.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);

  }

  public void test45() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test45");


    org.junit.experimental.categories.ExcludeCategories var0 = new org.junit.experimental.categories.ExcludeCategories();

  }

  public void test46() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test46");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.TestSuite var6 = new junit.framework.TestSuite(var3);
    junit.framework.AssertionFailedError var8 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var12 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var12);
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var12);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var15 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var8, "", (java.lang.Object[])var12);
    java.lang.String var16 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var15);
    var0.addError((junit.framework.Test)var6, (java.lang.Throwable)var15);
    junit.extensions.RepeatedTest var19 = new junit.extensions.RepeatedTest((junit.framework.Test)var6, 1);
    junit.extensions.ActiveTestSuite var20 = new junit.extensions.ActiveTestSuite();
    junit.extensions.ActiveTestSuite var21 = new junit.extensions.ActiveTestSuite();
    junit.framework.TestResult var22 = new junit.framework.TestResult();
    junit.framework.TestResult var23 = new junit.framework.TestResult();
    java.lang.Class[] var26 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var27 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var26);
    org.junit.experimental.categories.Categories.CategoryFilter var28 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var26);
    junit.framework.TestSuite var29 = new junit.framework.TestSuite(var26);
    junit.framework.AssertionFailedError var31 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var35 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var36 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var35);
    org.junit.experimental.categories.Categories.CategoryFilter var37 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var35);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var38 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var31, "", (java.lang.Object[])var35);
    java.lang.String var39 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var38);
    var23.addError((junit.framework.Test)var29, (java.lang.Throwable)var38);
    java.lang.String var41 = var29.getName();
    var22.endTest((junit.framework.Test)var29);
    java.util.Enumeration var43 = var22.errors();
    var20.runTest((junit.framework.Test)var21, var22);
    var19.run(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var16.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var39.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var43);

  }

  public void test47() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test47");


    junit.framework.AssertionFailedError var1 = new junit.framework.AssertionFailedError();
    org.junit.AssumptionViolatedException var2 = new org.junit.AssumptionViolatedException("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", (java.lang.Throwable)var1);

  }

  public void test48() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test48");


    org.junit.runner.Result var1 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var2 = var1.createListener();
    org.junit.runner.Computer var3 = new org.junit.runner.Computer();
    java.lang.Class[] var6 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var6);
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var6);
    junit.framework.TestSuite var9 = new junit.framework.TestSuite(var6);
    org.junit.runner.Result var10 = org.junit.runner.JUnitCore.runClasses(var3, var6);
    var2.testRunFinished(var10);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.failNotSame("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object)var10, (java.lang.Object)1.0d);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);

  }

  public void test49() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test49");


    org.junit.experimental.max.MaxCore var1 = org.junit.experimental.max.MaxCore.forFolder("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n");
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);

  }

  public void test50() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test50");


    org.junit.runner.Result var0 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var1 = var0.createListener();
    org.junit.runner.Computer var2 = new org.junit.runner.Computer();
    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var5);
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var5);
    junit.framework.TestSuite var8 = new junit.framework.TestSuite(var5);
    org.junit.runner.Result var9 = org.junit.runner.JUnitCore.runClasses(var2, var5);
    var1.testRunFinished(var9);
    org.junit.runner.Description var14 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var18 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var14.addChild(var18);
    java.util.ArrayList var20 = var18.getChildren();
    var1.testIgnored(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);

  }

  public void test51() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test51");


    org.junit.runner.Computer var0 = new org.junit.runner.Computer();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.TestSuite var6 = new junit.framework.TestSuite(var3);
    org.junit.runner.Result var7 = org.junit.runner.JUnitCore.runClasses(var0, var3);
    java.lang.Class[] var11 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var12 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var11);
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var11);
    junit.framework.AssertionFailedError var15 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var19 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var20 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var19);
    org.junit.experimental.categories.Categories.CategoryFilter var21 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var19);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var22 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var15, "", (java.lang.Object[])var19);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var11, (java.lang.Object[])var19);
    java.lang.Class[] var27 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var28 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var27);
    org.junit.experimental.categories.Categories.CategoryFilter var29 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var27);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var27);
    org.junit.experimental.categories.Categories.CategoryFilter var31 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var27);
    org.junit.Assert.assertEquals((java.lang.Object[])var19, (java.lang.Object[])var27);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertSame((java.lang.Object)var3, (java.lang.Object)var19);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);

  }

  public void test52() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test52");


    junit.framework.JUnit4TestAdapterCache var1 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var2 = var1.toString();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    boolean var6 = var1.containsValue((java.lang.Object)var4);
    org.junit.Assert.assertNotEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>", (java.lang.Object)var1, (java.lang.Object)1.0d);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "{}"+ "'", var2.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);

  }

  public void test53() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test53");


    junit.framework.Assert.assertNotNull((java.lang.Object)(short)0);

  }

  public void test54() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test54");


    org.junit.rules.RuleChain var0 = org.junit.rules.RuleChain.emptyRuleChain();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);

  }

  public void test55() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test55");


    org.junit.rules.Timeout var1 = new org.junit.rules.Timeout(1);

  }

  public void test56() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test56");


    junit.framework.JUnit4TestAdapterCache var1 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var2 = var1.toString();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    boolean var6 = var1.containsValue((java.lang.Object)var4);
    junit.framework.JUnit4TestAdapterCache var7 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.Object var8 = var7.clone();
    var1.putAll((java.util.Map)var7);
    java.util.Set var10 = var7.entrySet();
    java.lang.String var11 = org.junit.experimental.theories.internal.ParameterizedAssertionError.join("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", (java.util.Collection)var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "{}"+ "'", var2.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + ""+ "'", var11.equals(""));

  }

  public void test57() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test57");


    org.junit.runner.Computer var0 = new org.junit.runner.Computer();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.TestSuite var6 = new junit.framework.TestSuite(var3);
    org.junit.runner.Result var7 = org.junit.runner.JUnitCore.runClasses(var0, var3);
    boolean var8 = var7.wasSuccessful();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == true);

  }

  public void test58() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test58");


    char[] var4 = new char[] { '#', ' ', 'a'};
    char[] var8 = new char[] { ' ', '#', ' '};
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.Assert.assertArrayEquals("", var4, var8);
      fail("Expected exception of type org.junit.internal.ArrayComparisonFailure");
    } catch (org.junit.internal.ArrayComparisonFailure e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);

  }

  public void test59() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test59");


    org.junit.runner.Result var0 = new org.junit.runner.Result();
    int var1 = var0.getFailureCount();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var1 == 0);

  }

  public void test60() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test60");


    java.lang.annotation.Annotation[] var3 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var4 = org.junit.runner.Description.createSuiteDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>", var3);
    org.junit.runner.Description var5 = org.junit.runner.Description.createTestDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", var3);
    org.junit.runner.Description var6 = var5.childlessCopy();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);

  }

  public void test61() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test61");


    junit.extensions.ActiveTestSuite var1 = new junit.extensions.ActiveTestSuite("{}");

  }

  public void test62() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test62");


    int[] var3 = new int[] { 1, 1};
    int[] var6 = new int[] { 100, 100};
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>", var3, var6);
      fail("Expected exception of type org.junit.internal.ArrayComparisonFailure");
    } catch (org.junit.internal.ArrayComparisonFailure e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);

  }

  public void test63() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test63");


    org.junit.runner.Runner var0 = org.junit.runners.Suite.emptySuite();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);

  }

  public void test64() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test64");


    org.junit.AssumptionViolatedException var1 = new org.junit.AssumptionViolatedException("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n");
    junit.framework.TestResult var2 = new junit.framework.TestResult();
    junit.framework.TestResult var3 = new junit.framework.TestResult();
    java.lang.Class[] var6 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var6);
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var6);
    junit.framework.TestSuite var9 = new junit.framework.TestSuite(var6);
    junit.framework.AssertionFailedError var11 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var15 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var15);
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var15);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var18 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var11, "", (java.lang.Object[])var15);
    java.lang.String var19 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var18);
    var3.addError((junit.framework.Test)var9, (java.lang.Throwable)var18);
    java.lang.String var21 = var9.getName();
    var2.endTest((junit.framework.Test)var9);
    java.util.Enumeration var23 = var2.errors();
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertSame((java.lang.Object)"org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", (java.lang.Object)var23);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var19.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);

  }

  public void test65() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test65");


    junit.extensions.ActiveTestSuite var0 = new junit.extensions.ActiveTestSuite();
    junit.extensions.ActiveTestSuite var1 = new junit.extensions.ActiveTestSuite();
    junit.framework.TestResult var2 = new junit.framework.TestResult();
    junit.framework.TestResult var3 = new junit.framework.TestResult();
    java.lang.Class[] var6 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var6);
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var6);
    junit.framework.TestSuite var9 = new junit.framework.TestSuite(var6);
    junit.framework.AssertionFailedError var11 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var15 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var15);
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var15);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var18 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var11, "", (java.lang.Object[])var15);
    java.lang.String var19 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var18);
    var3.addError((junit.framework.Test)var9, (java.lang.Throwable)var18);
    java.lang.String var21 = var9.getName();
    var2.endTest((junit.framework.Test)var9);
    java.util.Enumeration var23 = var2.errors();
    var0.runTest((junit.framework.Test)var1, var2);
    org.junit.internal.runners.JUnit38ClassRunner var25 = new org.junit.internal.runners.JUnit38ClassRunner((junit.framework.Test)var0);
    int var26 = var0.testCount();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var19.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == 0);

  }

  public void test66() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test66");


    org.junit.Assert.assertFalse(false);

  }

  public void test67() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test67");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.runners.MethodSorters var1 = org.junit.runners.MethodSorters.valueOf("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n");
      fail("Expected exception of type java.lang.IllegalArgumentException");
    } catch (java.lang.IllegalArgumentException e) {
      // Expected exception.
    }

  }

  public void test68() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test68");


    org.junit.runner.Computer var0 = org.junit.experimental.ParallelComputer.methods();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);

  }

  public void test69() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test69");


    org.junit.Assert.assertNotEquals("hi!", (-1L), 1L);

  }

  public void test70() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test70");


    org.junit.runner.manipulation.NoTestsRemainException var0 = new org.junit.runner.manipulation.NoTestsRemainException();

  }

  public void test71() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test71");


    org.junit.runner.Computer var0 = new org.junit.runner.Computer();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.TestSuite var6 = new junit.framework.TestSuite(var3);
    org.junit.runner.Result var7 = org.junit.runner.JUnitCore.runClasses(var0, var3);
    org.junit.runner.Computer var8 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var9 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var11 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var12 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var11);
    org.junit.runner.Runner var13 = var8.getSuite((org.junit.runners.model.RunnerBuilder)var9, var11);
    org.junit.runner.Result var14 = org.junit.runner.JUnitCore.runClasses(var0, var11);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.include(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);

  }

  public void test72() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test72");


    org.junit.rules.TemporaryFolder var0 = new org.junit.rules.TemporaryFolder();
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      java.io.File var2 = var0.newFile("categories [all]");
      fail("Expected exception of type java.lang.IllegalStateException");
    } catch (java.lang.IllegalStateException e) {
      // Expected exception.
    }

  }

  public void test73() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test73");


    junit.framework.Assert.assertNotNull((java.lang.Object)1.0f);

  }

  public void test74() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test74");


    byte[] var1 = new byte[] { };
    byte[] var2 = new byte[] { };
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", var1, var2);
    byte[] var5 = new byte[] { };
    byte[] var6 = new byte[] { };
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", var5, var6);
    org.junit.Assert.assertArrayEquals(var2, var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);

  }

  public void test75() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test75");


    org.junit.experimental.categories.IncludeCategories var0 = new org.junit.experimental.categories.IncludeCategories();

  }

  public void test76() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test76");


    org.junit.validator.AnnotationValidatorFactory var0 = new org.junit.validator.AnnotationValidatorFactory();

  }

  public void test77() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test77");


    org.junit.runner.Result var0 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var1 = var0.createListener();
    org.junit.runner.Description var5 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var9 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var5.addChild(var9);
    var1.testFinished(var5);
    org.junit.runner.Result var12 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var13 = var12.createListener();
    org.junit.runner.Description var17 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var21 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var17.addChild(var21);
    junit.framework.Assert.assertNotSame((java.lang.Object)var13, (java.lang.Object)var17);
    org.junit.runner.Description var27 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var31 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var27.addChild(var31);
    var13.testIgnored(var31);
    var1.testFinished(var31);
    junit.framework.JUnit4TestAdapterCache var35 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var36 = var35.toString();
    java.lang.Class[] var38 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var39 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var38);
    boolean var40 = var35.containsValue((java.lang.Object)var38);
    org.junit.runner.Result var41 = org.junit.runner.JUnitCore.runClasses(var38);
    var1.testRunFinished(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "{}"+ "'", var36.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);

  }

  public void test78() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test78");


    org.junit.experimental.max.MaxCore var1 = org.junit.experimental.max.MaxCore.forFolder("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n");
    org.junit.runner.Computer var2 = new org.junit.runner.Computer();
    java.lang.Class[] var6 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var6);
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var6);
    junit.framework.AssertionFailedError var10 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var14 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var14);
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var14);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var17 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var10, "", (java.lang.Object[])var14);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var6, (java.lang.Object[])var14);
    java.lang.Class[] var22 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var23 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var22);
    org.junit.experimental.categories.Categories.CategoryFilter var24 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var22);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var22);
    org.junit.experimental.categories.Categories.CategoryFilter var26 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var22);
    org.junit.Assert.assertEquals((java.lang.Object[])var14, (java.lang.Object[])var22);
    org.junit.runner.Request var28 = org.junit.runner.Request.classes(var2, var22);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.runner.Request var29 = var1.sortRequest(var28);
      fail("Expected exception of type java.lang.RuntimeException");
    } catch (java.lang.RuntimeException e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);

  }

  public void test79() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test79");


    junit.framework.Assert.assertEquals("categories [all]", "", "");

  }

  public void test80() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test80");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    java.util.Collection var6 = var0.values();
    int var7 = var0.size();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == 0);

  }

  public void test81() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test81");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    java.util.Collection var6 = var0.values();
    boolean var8 = var0.equals((java.lang.Object)true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);

  }

  public void test82() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test82");


    junit.runner.BaseTestRunner.savePreferences();

  }

  public void test83() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test83");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.TestSuite var6 = new junit.framework.TestSuite(var3);
    junit.framework.AssertionFailedError var8 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var12 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var12);
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var12);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var15 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var8, "", (java.lang.Object[])var12);
    java.lang.String var16 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var15);
    var0.addError((junit.framework.Test)var6, (java.lang.Throwable)var15);
    junit.extensions.RepeatedTest var19 = new junit.extensions.RepeatedTest((junit.framework.Test)var6, 1);
    java.util.Enumeration var20 = var6.tests();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var16.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);

  }

  public void test84() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test84");


    org.junit.rules.Timeout.Builder var0 = org.junit.rules.Timeout.builder();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);

  }

  public void test85() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test85");


    org.junit.Assert.assertEquals(100.0f, 100.0f, 100.0f);

  }

  public void test86() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test86");


    junit.framework.Assert.assertEquals((short)(-1), (short)(-1));

  }

  public void test87() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test87");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertNull((java.lang.Object)0L);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test88() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test88");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", "categories [all]");
      fail("Expected exception of type junit.framework.ComparisonFailure");
    } catch (junit.framework.ComparisonFailure e) {
      // Expected exception.
    }

  }

  public void test89() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test89");


    java.lang.String var1 = junit.runner.BaseTestRunner.getPreference("");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);

  }

  public void test90() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test90");


    org.junit.rules.ExpectedException var0 = org.junit.rules.ExpectedException.none();
    org.junit.rules.RuleChain var1 = org.junit.rules.RuleChain.outerRule((org.junit.rules.TestRule)var0);
    org.junit.rules.ExpectedException var2 = org.junit.rules.ExpectedException.none();
    org.junit.rules.RuleChain var3 = org.junit.rules.RuleChain.outerRule((org.junit.rules.TestRule)var2);
    org.junit.rules.RuleChain var4 = var1.around((org.junit.rules.TestRule)var2);
    org.junit.rules.ExpectedException var5 = var2.handleAssumptionViolatedExceptions();
    var5.expectMessage("categories [all]");
    org.junit.rules.DisableOnDebug var8 = new org.junit.rules.DisableOnDebug((org.junit.rules.TestRule)var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);

  }

  public void test91() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test91");


    org.junit.experimental.max.MaxCore var1 = org.junit.experimental.max.MaxCore.forFolder("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>");
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);

  }

  public void test92() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test92");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    junit.framework.TestResult var1 = new junit.framework.TestResult();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.TestSuite var7 = new junit.framework.TestSuite(var4);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    java.lang.String var17 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var16);
    var1.addError((junit.framework.Test)var7, (java.lang.Throwable)var16);
    java.lang.String var19 = var7.getName();
    var0.endTest((junit.framework.Test)var7);
    java.util.Enumeration var21 = var0.errors();
    boolean var22 = var0.shouldStop();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var17.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);

  }

  public void test93() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test93");


    junit.framework.JUnit4TestAdapterCache var1 = new junit.framework.JUnit4TestAdapterCache();
    boolean var3 = var1.containsKey((java.lang.Object)false);
    int var4 = var1.size();
    java.lang.Class[] var7 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var7);
    org.junit.experimental.categories.Categories.CategoryFilter var9 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var7);
    java.lang.String var10 = junit.framework.Assert.format("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object)var1, (java.lang.Object)true);
    java.util.Collection var11 = var1.values();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>"+ "'", var10.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);

  }

  public void test94() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test94");


    org.junit.runner.Description var3 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    boolean var4 = var3.isTest();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == true);

  }

  public void test95() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test95");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals(0, 10);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test96() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test96");


    junit.framework.Assert.assertEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", true, true);

  }

  public void test97() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test97");


    org.junit.internal.builders.JUnit4Builder var0 = new org.junit.internal.builders.JUnit4Builder();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.AssertionFailedError var8 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var12 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var12);
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var12);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var15 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var8, "", (java.lang.Object[])var12);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var4, (java.lang.Object[])var12);
    java.lang.Class[] var20 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var21 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var20);
    org.junit.experimental.categories.Categories.CategoryFilter var22 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var20);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var20);
    org.junit.experimental.categories.Categories.CategoryFilter var24 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var20);
    org.junit.Assert.assertEquals((java.lang.Object[])var12, (java.lang.Object[])var20);
    org.junit.runners.Suite var26 = new org.junit.runners.Suite((org.junit.runners.model.RunnerBuilder)var0, var20);
    java.lang.Class[] var28 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var29 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var28);
    java.lang.String var30 = var29.describe();
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      var26.filter((org.junit.runner.manipulation.Filter)var29);
      fail("Expected exception of type org.junit.runner.manipulation.NoTestsRemainException");
    } catch (org.junit.runner.manipulation.NoTestsRemainException e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "categories [all]"+ "'", var30.equals("categories [all]"));

  }

  public void test98() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test98");


    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.AssertionFailedError var8 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var12 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var12);
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var12);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var15 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var8, "", (java.lang.Object[])var12);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var4, (java.lang.Object[])var12);
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(var12);
    java.lang.String var18 = org.junit.experimental.theories.internal.ParameterizedAssertionError.join("{}", (java.lang.Object[])var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + ""+ "'", var18.equals(""));

  }

  public void test99() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test99");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    junit.framework.JUnit4TestAdapterCache var6 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.Object var7 = var6.clone();
    var0.putAll((java.util.Map)var6);
    java.lang.Object var9 = var0.clone();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);

  }

  public void test100() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test100");


    junit.framework.JUnit4TestAdapterCache var1 = junit.framework.JUnit4TestAdapterCache.getDefault();
    org.junit.runner.Result var2 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var3 = var2.createListener();
    org.junit.runner.Description var7 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var11 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var7.addChild(var11);
    var3.testFinished(var7);
    org.junit.runner.Result var14 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var15 = var14.createListener();
    org.junit.runner.Description var19 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var23 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var19.addChild(var23);
    junit.framework.Assert.assertNotSame((java.lang.Object)var15, (java.lang.Object)var19);
    org.junit.runner.Description var29 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var33 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var29.addChild(var33);
    var15.testIgnored(var33);
    var3.testFinished(var33);
    org.junit.Assert.assertNotEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@1f489d9> but was:<100>", (java.lang.Object)var1, (java.lang.Object)var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);

  }

  public void test101() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test101");


    org.junit.runner.Description var3 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var7 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var3.addChild(var7);
    org.junit.runner.manipulation.Filter var9 = org.junit.runner.manipulation.Filter.matchMethodDescription(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);

  }

  public void test102() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test102");


    org.junit.runner.Computer var0 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var1 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var3);
    org.junit.runner.Runner var5 = var0.getSuite((org.junit.runners.model.RunnerBuilder)var1, var3);
    int var6 = var5.testCount();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == 1);

  }

  public void test103() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test103");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("hi!", 100.0f, 10.0f, 0.0f);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test104() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test104");


    junit.framework.AssertionFailedError var1 = new junit.framework.AssertionFailedError();
    org.junit.experimental.theories.PotentialAssignment.CouldNotGenerateValueException var2 = new org.junit.experimental.theories.PotentialAssignment.CouldNotGenerateValueException((java.lang.Throwable)var1);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.Assume.assumeNoException("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@1f489d9> but was:<100>", (java.lang.Throwable)var2);
      fail("Expected exception of type org.junit.AssumptionViolatedException");
    } catch (org.junit.AssumptionViolatedException e) {
      // Expected exception.
    }

  }

  public void test105() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test105");


    junit.extensions.ActiveTestSuite var0 = new junit.extensions.ActiveTestSuite();
    junit.extensions.ActiveTestSuite var1 = new junit.extensions.ActiveTestSuite();
    junit.framework.TestResult var2 = new junit.framework.TestResult();
    junit.framework.TestResult var3 = new junit.framework.TestResult();
    java.lang.Class[] var6 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var6);
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var6);
    junit.framework.TestSuite var9 = new junit.framework.TestSuite(var6);
    junit.framework.AssertionFailedError var11 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var15 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var15);
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var15);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var18 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var11, "", (java.lang.Object[])var15);
    java.lang.String var19 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var18);
    var3.addError((junit.framework.Test)var9, (java.lang.Throwable)var18);
    java.lang.String var21 = var9.getName();
    var2.endTest((junit.framework.Test)var9);
    java.util.Enumeration var23 = var2.errors();
    var0.runTest((junit.framework.Test)var1, var2);
    org.junit.internal.runners.JUnit38ClassRunner var25 = new org.junit.internal.runners.JUnit38ClassRunner((junit.framework.Test)var0);
    org.junit.runner.Result var26 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var27 = var26.createListener();
    org.junit.runner.Description var31 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var35 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var31.addChild(var35);
    var27.testFinished(var31);
    org.junit.runner.manipulation.Filter var38 = org.junit.runner.manipulation.Filter.matchMethodDescription(var31);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      var25.filter(var38);
      fail("Expected exception of type org.junit.runner.manipulation.NoTestsRemainException");
    } catch (org.junit.runner.manipulation.NoTestsRemainException e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var19.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var38);

  }

  public void test106() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test106");


    org.junit.runner.Description var3 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var7 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var3.addChild(var7);
    java.util.ArrayList var9 = var7.getChildren();
    org.junit.runners.model.MultipleFailureException var10 = new org.junit.runners.model.MultipleFailureException((java.util.List)var9);
    org.junit.runners.model.MultipleFailureException var11 = new org.junit.runners.model.MultipleFailureException((java.util.List)var9);
    junit.framework.JUnit4TestAdapterCache var12 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var13 = var12.toString();
    java.lang.Class[] var15 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var15);
    boolean var17 = var12.containsValue((java.lang.Object)var15);
    junit.framework.JUnit4TestAdapterCache var18 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.Object var19 = var18.clone();
    var12.putAll((java.util.Map)var18);
    java.util.Set var21 = var18.entrySet();
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertSame((java.lang.Object)var9, (java.lang.Object)var18);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "{}"+ "'", var13.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);

  }

  public void test107() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test107");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("", (-1.0d), 10.0d, 10.0d);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test108() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test108");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    junit.framework.JUnit4TestAdapterCache var6 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.Object var7 = var6.clone();
    var0.putAll((java.util.Map)var6);
    java.util.Set var9 = var6.entrySet();
    org.junit.rules.ExpectedException var10 = org.junit.rules.ExpectedException.none();
    boolean var11 = var6.containsValue((java.lang.Object)var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);

  }

  public void test109() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test109");


    junit.framework.Assert.assertEquals("hi!", false, false);

  }

  public void test110() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test110");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@1f489d9> but was:<100>", (short)0, (short)100);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test111() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test111");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>");
      fail("Expected exception of type junit.framework.ComparisonFailure");
    } catch (junit.framework.ComparisonFailure e) {
      // Expected exception.
    }

  }

  public void test112() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test112");


    org.junit.rules.TemporaryFolder var0 = new org.junit.rules.TemporaryFolder();
    var0.delete();

  }

  public void test113() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test113");


    org.junit.rules.Timeout var1 = org.junit.rules.Timeout.seconds((-1L));
    org.junit.rules.RuleChain var2 = org.junit.rules.RuleChain.outerRule((org.junit.rules.TestRule)var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);

  }

  public void test114() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test114");


    java.lang.String var1 = junit.runner.BaseTestRunner.truncate("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unk..."+ "'", var1.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unk..."));

  }

  public void test115() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test115");


    java.lang.Class[] var2 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var3 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var2);
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var2);
    java.lang.Class[] var7 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var7);
    org.junit.experimental.categories.Categories.CategoryFilter var9 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var7);
    org.junit.runner.manipulation.Filter var10 = var4.intersect((org.junit.runner.manipulation.Filter)var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);

  }

  public void test116() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test116");


    org.junit.Assume.assumeTrue("categories [all]", true);

  }

  public void test117() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test117");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    int var1 = var0.failureCount();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var1 == 0);

  }

  public void test118() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test118");


    org.junit.Assert.assertEquals(0.0d, 0.0d, 10.0d);

  }

  public void test119() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test119");


    org.junit.Assume.assumeTrue(true);

  }

  public void test120() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test120");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    org.junit.runner.Result var6 = org.junit.runner.JUnitCore.runClasses(var3);
    org.junit.runner.Computer var7 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var8 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var10 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var11 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var10);
    org.junit.runner.Runner var12 = var7.getSuite((org.junit.runners.model.RunnerBuilder)var8, var10);
    org.junit.Assert.assertArrayEquals((java.lang.Object[])var3, (java.lang.Object[])var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);

  }

  public void test121() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test121");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    org.junit.runner.Description var9 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var13 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var9.addChild(var13);
    java.util.ArrayList var15 = var13.getChildren();
    java.util.List var16 = var0.asTestList(var13);
    int var17 = var0.size();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == 1);

  }

  public void test122() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test122");


    java.lang.String var0 = junit.runner.Version.id();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var0 + "' != '" + "4.13-SNAPSHOT"+ "'", var0.equals("4.13-SNAPSHOT"));

  }

  public void test123() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test123");


    org.junit.runner.Computer var0 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var1 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var3);
    org.junit.runner.Runner var5 = var0.getSuite((org.junit.runners.model.RunnerBuilder)var1, var3);
    org.junit.runner.Computer var6 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var7 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var9 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var10 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var9);
    org.junit.runner.Runner var11 = var6.getSuite((org.junit.runners.model.RunnerBuilder)var7, var9);
    org.junit.internal.builders.IgnoredBuilder var12 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var16 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var16);
    org.junit.experimental.categories.Categories.CategoryFilter var18 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var16);
    junit.framework.AssertionFailedError var20 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var24 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var25 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var24);
    org.junit.experimental.categories.Categories.CategoryFilter var26 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var24);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var27 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var20, "", (java.lang.Object[])var24);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var16, (java.lang.Object[])var24);
    java.lang.Class[] var32 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var33 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var32);
    org.junit.experimental.categories.Categories.CategoryFilter var34 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var32);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var32);
    org.junit.experimental.categories.Categories.CategoryFilter var36 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var32);
    org.junit.Assert.assertEquals((java.lang.Object[])var24, (java.lang.Object[])var32);
    org.junit.runner.Runner var38 = var6.getSuite((org.junit.runners.model.RunnerBuilder)var12, var24);
    org.junit.runner.Result var39 = org.junit.runner.JUnitCore.runClasses(var0, var24);
    int var40 = var39.getRunCount();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == 0);

  }

  public void test124() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test124");


    org.hamcrest.Matcher var0 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    org.hamcrest.Matcher var1 = org.junit.matchers.JUnitMatchers.hasItem(var0);
    org.hamcrest.Matcher var2 = org.junit.internal.matchers.ThrowableMessageMatcher.<java.lang.Throwable>hasMessage(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);

  }

  public void test125() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test125");


    org.hamcrest.Matcher var0 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    org.junit.internal.matchers.ThrowableMessageMatcher var1 = new org.junit.internal.matchers.ThrowableMessageMatcher(var0);
    var1._dont_implement_Matcher___instead_extend_BaseMatcher_();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);

  }

  public void test126() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test126");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    junit.framework.TestResult var1 = new junit.framework.TestResult();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.TestSuite var7 = new junit.framework.TestSuite(var4);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    java.lang.String var17 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var16);
    var1.addError((junit.framework.Test)var7, (java.lang.Throwable)var16);
    java.lang.String var19 = var7.getName();
    var0.endTest((junit.framework.Test)var7);
    junit.framework.TestResult var21 = new junit.framework.TestResult();
    java.lang.Class[] var24 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var25 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var24);
    org.junit.experimental.categories.Categories.CategoryFilter var26 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var24);
    junit.framework.TestSuite var27 = new junit.framework.TestSuite(var24);
    junit.framework.AssertionFailedError var29 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var33 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var34 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var33);
    org.junit.experimental.categories.Categories.CategoryFilter var35 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var33);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var36 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var29, "", (java.lang.Object[])var33);
    java.lang.String var37 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var36);
    var21.addError((junit.framework.Test)var27, (java.lang.Throwable)var36);
    junit.extensions.RepeatedTest var40 = new junit.extensions.RepeatedTest((junit.framework.Test)var27, 1);
    junit.framework.TestResult var41 = new junit.framework.TestResult();
    java.lang.Class[] var44 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var45 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var44);
    org.junit.experimental.categories.Categories.CategoryFilter var46 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var44);
    junit.framework.TestSuite var47 = new junit.framework.TestSuite(var44);
    junit.framework.AssertionFailedError var49 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var53 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var54 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var53);
    org.junit.experimental.categories.Categories.CategoryFilter var55 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var53);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var56 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var49, "", (java.lang.Object[])var53);
    java.lang.String var57 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var56);
    var41.addError((junit.framework.Test)var47, (java.lang.Throwable)var56);
    var7.runTest((junit.framework.Test)var40, var41);
    junit.framework.AssertionFailedError var61 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var65 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var66 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var65);
    org.junit.experimental.categories.Categories.CategoryFilter var67 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var65);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var68 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var61, "", (java.lang.Object[])var65);
    junit.framework.TestFailure var69 = new junit.framework.TestFailure((junit.framework.Test)var7, (java.lang.Throwable)var68);
    java.lang.String var70 = var7.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var17.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var37.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var57.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var65);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var66);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var67);

  }

  public void test127() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test127");


    org.junit.rules.TestWatchman var0 = new org.junit.rules.TestWatchman();

  }

  public void test128() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test128");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    boolean var2 = var0.equals((java.lang.Object)"categories [all]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);

  }

  public void test129() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test129");


    org.junit.rules.Timeout var1 = org.junit.rules.Timeout.seconds(10L);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);

  }

  public void test130() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test130");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    boolean var2 = var0.containsKey((java.lang.Object)false);
    java.lang.String var3 = var0.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "{}"+ "'", var3.equals("{}"));

  }

  public void test131() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test131");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.Object var1 = var0.clone();
    java.lang.Object var3 = var0.remove((java.lang.Object)(short)0);
    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var5);
    java.lang.String var7 = var6.describe();
    boolean var8 = var0.equals((java.lang.Object)var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "categories [all]"+ "'", var7.equals("categories [all]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);

  }

  public void test132() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test132");


    java.lang.annotation.Annotation[] var1 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var2 = org.junit.runner.Description.createSuiteDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);

  }

  public void test133() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test133");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.TestSuite var6 = new junit.framework.TestSuite(var3);
    junit.framework.AssertionFailedError var8 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var12 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var12);
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var12);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var15 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var8, "", (java.lang.Object[])var12);
    java.lang.String var16 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var15);
    var0.addError((junit.framework.Test)var6, (java.lang.Throwable)var15);
    junit.extensions.RepeatedTest var19 = new junit.extensions.RepeatedTest((junit.framework.Test)var6, 1);
    java.lang.String var20 = var19.toString();
    junit.framework.Test var21 = var19.getTest();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var16.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);

  }

  public void test134() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test134");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.failSame("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n");
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test135() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test135");


    org.junit.Assert.assertEquals(100.0d, 100.0d, 100.0d);

  }

  public void test136() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test136");


    org.hamcrest.Matcher var1 = org.junit.experimental.results.ResultMatchers.hasFailureContaining("{}");
    org.hamcrest.Matcher var2 = org.junit.internal.matchers.StacktracePrintingMatcher.<java.lang.Throwable>isThrowable(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);

  }

  public void test137() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test137");


    org.junit.runner.Result var0 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var1 = var0.createListener();
    org.junit.runner.Computer var2 = new org.junit.runner.Computer();
    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var5);
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var5);
    junit.framework.TestSuite var8 = new junit.framework.TestSuite(var5);
    org.junit.runner.Result var9 = org.junit.runner.JUnitCore.runClasses(var2, var5);
    var1.testRunFinished(var9);
    org.junit.Assert.assertNotNull((java.lang.Object)var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);

  }

  public void test138() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test138");


    org.junit.runners.model.InitializationError var1 = new org.junit.runners.model.InitializationError("hi!");

  }

  public void test139() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test139");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    boolean var2 = var0.containsKey((java.lang.Object)false);
    int var3 = var0.size();
    java.lang.String var4 = var0.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "{}"+ "'", var4.equals("{}"));

  }

  public void test140() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test140");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    org.hamcrest.Matcher var2 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    org.junit.internal.matchers.ThrowableMessageMatcher var3 = new org.junit.internal.matchers.ThrowableMessageMatcher(var2);
    org.junit.internal.matchers.ThrowableCauseMatcher var4 = new org.junit.internal.matchers.ThrowableCauseMatcher((org.hamcrest.Matcher)var3);
    java.lang.Object var5 = var0.remove((java.lang.Object)var4);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals((java.lang.Object)var0, (java.lang.Object)'a');
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test141() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test141");


    junit.framework.JUnit4TestAdapterCache var1 = new junit.framework.JUnit4TestAdapterCache();
    boolean var3 = var1.containsKey((java.lang.Object)false);
    int var4 = var1.size();
    java.lang.Class[] var7 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var7);
    org.junit.experimental.categories.Categories.CategoryFilter var9 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var7);
    java.lang.String var10 = junit.framework.Assert.format("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object)var1, (java.lang.Object)true);
    org.junit.runner.Description var14 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var18 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var14.addChild(var18);
    java.util.ArrayList var20 = var18.getChildren();
    org.junit.runners.model.MultipleFailureException var21 = new org.junit.runners.model.MultipleFailureException((java.util.List)var20);
    org.junit.internal.runners.model.MultipleFailureException var22 = new org.junit.internal.runners.model.MultipleFailureException((java.util.List)var20);
    org.hamcrest.Matcher var23 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    org.junit.internal.matchers.ThrowableMessageMatcher var24 = new org.junit.internal.matchers.ThrowableMessageMatcher(var23);
    java.lang.Object var25 = var1.put((java.lang.Object)var22, (java.lang.Object)var24);
    org.junit.experimental.max.CouldNotReadCoreException var26 = new org.junit.experimental.max.CouldNotReadCoreException((java.lang.Throwable)var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>"+ "'", var10.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);

  }

  public void test142() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test142");


    org.hamcrest.Matcher var1 = org.junit.experimental.results.ResultMatchers.failureCountIs(0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);

  }

  public void test143() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test143");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.failSame("{}");
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test144() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test144");


    org.junit.runner.Description var4 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var8 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var4.addChild(var8);
    java.util.ArrayList var10 = var8.getChildren();
    junit.framework.Assert.assertNotNull((java.lang.Object)var10);
    org.hamcrest.Matcher var12 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    org.hamcrest.Matcher var13 = org.junit.matchers.JUnitMatchers.hasItem(var12);
    org.hamcrest.Matcher var14 = org.junit.internal.matchers.StacktracePrintingMatcher.<java.lang.Throwable>isThrowable(var13);
    org.junit.AssumptionViolatedException var15 = new org.junit.AssumptionViolatedException("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@1f489d9> but was:<100>", (java.lang.Object)var10, var14);
    org.hamcrest.Matcher var16 = org.junit.internal.matchers.StacktracePrintingMatcher.<java.lang.Throwable>isThrowable(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);

  }

  public void test145() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test145");


    junit.framework.Assert.assertEquals(false, false);

  }

  public void test146() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test146");


    java.lang.annotation.Annotation[] var2 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var3 = org.junit.runner.Description.createSuiteDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", var2);
    boolean var4 = var3.isTest();
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertNull("categories [all]", (java.lang.Object)var4);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == true);

  }

  public void test147() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test147");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.TestSuite var6 = new junit.framework.TestSuite(var3);
    junit.framework.AssertionFailedError var8 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var12 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var12);
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var12);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var15 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var8, "", (java.lang.Object[])var12);
    java.lang.String var16 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var15);
    var0.addError((junit.framework.Test)var6, (java.lang.Throwable)var15);
    org.junit.runners.model.InitializationError var18 = new org.junit.runners.model.InitializationError((java.lang.Throwable)var15);
    org.junit.internal.runners.statements.Fail var19 = new org.junit.internal.runners.statements.Fail((java.lang.Throwable)var15);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      var19.evaluate();
      fail("Expected exception of type org.junit.experimental.theories.internal.ParameterizedAssertionError");
    } catch (org.junit.experimental.theories.internal.ParameterizedAssertionError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var16.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));

  }

  public void test148() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test148");


    java.lang.annotation.Annotation[] var2 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var3 = org.junit.runner.Description.createTestDescription("hi!", "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", var2);
    junit.framework.AssertionFailedError var5 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var9 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var10 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var9);
    org.junit.experimental.categories.Categories.CategoryFilter var11 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var9);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var12 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var5, "", (java.lang.Object[])var9);
    java.lang.String var13 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var12);
    junit.framework.TestResult var14 = new junit.framework.TestResult();
    java.lang.Class[] var17 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var18 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var17);
    org.junit.experimental.categories.Categories.CategoryFilter var19 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var17);
    junit.framework.TestSuite var20 = new junit.framework.TestSuite(var17);
    junit.framework.AssertionFailedError var22 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var26 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var27 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var26);
    org.junit.experimental.categories.Categories.CategoryFilter var28 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var26);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var29 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var22, "", (java.lang.Object[])var26);
    java.lang.String var30 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var29);
    var14.addError((junit.framework.Test)var20, (java.lang.Throwable)var29);
    junit.extensions.RepeatedTest var33 = new junit.extensions.RepeatedTest((junit.framework.Test)var20, 1);
    boolean var34 = var12.equals((java.lang.Object)var33);
    org.junit.runner.notification.Failure var35 = new org.junit.runner.notification.Failure(var3, (java.lang.Throwable)var12);
    java.lang.String var36 = var35.getTrace();
    java.lang.Throwable var37 = var35.getException();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var13.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var30.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var36.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var37);

  }

  public void test149() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test149");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    junit.framework.TestResult var1 = new junit.framework.TestResult();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.TestSuite var7 = new junit.framework.TestSuite(var4);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    java.lang.String var17 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var16);
    var1.addError((junit.framework.Test)var7, (java.lang.Throwable)var16);
    java.lang.String var19 = var7.getName();
    var0.endTest((junit.framework.Test)var7);
    junit.framework.TestResult var21 = new junit.framework.TestResult();
    java.lang.Class[] var24 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var25 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var24);
    org.junit.experimental.categories.Categories.CategoryFilter var26 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var24);
    junit.framework.TestSuite var27 = new junit.framework.TestSuite(var24);
    junit.framework.AssertionFailedError var29 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var33 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var34 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var33);
    org.junit.experimental.categories.Categories.CategoryFilter var35 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var33);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var36 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var29, "", (java.lang.Object[])var33);
    java.lang.String var37 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var36);
    var21.addError((junit.framework.Test)var27, (java.lang.Throwable)var36);
    junit.extensions.RepeatedTest var40 = new junit.extensions.RepeatedTest((junit.framework.Test)var27, 1);
    junit.framework.TestResult var41 = new junit.framework.TestResult();
    java.lang.Class[] var44 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var45 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var44);
    org.junit.experimental.categories.Categories.CategoryFilter var46 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var44);
    junit.framework.TestSuite var47 = new junit.framework.TestSuite(var44);
    junit.framework.AssertionFailedError var49 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var53 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var54 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var53);
    org.junit.experimental.categories.Categories.CategoryFilter var55 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var53);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var56 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var49, "", (java.lang.Object[])var53);
    java.lang.String var57 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var56);
    var41.addError((junit.framework.Test)var47, (java.lang.Throwable)var56);
    var7.runTest((junit.framework.Test)var40, var41);
    java.util.Enumeration var60 = var41.errors();
    int var61 = var41.runCount();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var17.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var37.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var57.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var60);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == 0);

  }

  public void test150() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test150");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("hi!", (short)100, (short)(-1));
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test151() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test151");


    org.junit.rules.ExpectedException var1 = org.junit.rules.ExpectedException.none();
    org.junit.rules.RuleChain var2 = org.junit.rules.RuleChain.outerRule((org.junit.rules.TestRule)var1);
    org.junit.Assert.assertNotEquals((java.lang.Object)0L, (java.lang.Object)var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);

  }

  public void test152() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test152");


    org.junit.internal.builders.AllDefaultPossibilitiesBuilder var1 = new org.junit.internal.builders.AllDefaultPossibilitiesBuilder(false);

  }

  public void test153() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test153");


    java.lang.Class[] var2 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var3 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var2);
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var2);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var2);
    org.hamcrest.Matcher var7 = org.junit.matchers.JUnitMatchers.containsString("{}");
    org.junit.internal.AssumptionViolatedException var8 = new org.junit.internal.AssumptionViolatedException((java.lang.Object)var2, var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);

  }

  public void test154() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test154");


    org.junit.runner.Description var3 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Result var4 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var5 = var4.createListener();
    org.junit.runner.Description var9 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var13 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var9.addChild(var13);
    junit.framework.Assert.assertNotSame((java.lang.Object)var5, (java.lang.Object)var9);
    boolean var16 = var3.equals((java.lang.Object)var9);
    org.junit.runner.Computer var17 = new org.junit.runner.Computer();
    java.lang.Class[] var20 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var21 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var20);
    org.junit.experimental.categories.Categories.CategoryFilter var22 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var20);
    junit.framework.TestSuite var23 = new junit.framework.TestSuite(var20);
    org.junit.runner.Result var24 = org.junit.runner.JUnitCore.runClasses(var17, var20);
    org.junit.runner.Computer var25 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var26 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var28 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var29 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var28);
    org.junit.runner.Runner var30 = var25.getSuite((org.junit.runners.model.RunnerBuilder)var26, var28);
    org.junit.runner.Result var31 = org.junit.runner.JUnitCore.runClasses(var17, var28);
    org.junit.Assert.assertNotSame((java.lang.Object)var9, (java.lang.Object)var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);

  }

  public void test155() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test155");


    org.junit.rules.ExpectedException var0 = org.junit.rules.ExpectedException.none();
    org.junit.rules.RuleChain var1 = org.junit.rules.RuleChain.outerRule((org.junit.rules.TestRule)var0);
    org.junit.rules.ExpectedException var2 = org.junit.rules.ExpectedException.none();
    org.junit.rules.RuleChain var3 = org.junit.rules.RuleChain.outerRule((org.junit.rules.TestRule)var2);
    org.junit.rules.ExpectedException var4 = org.junit.rules.ExpectedException.none();
    org.junit.rules.RuleChain var5 = org.junit.rules.RuleChain.outerRule((org.junit.rules.TestRule)var4);
    org.junit.rules.RuleChain var6 = var3.around((org.junit.rules.TestRule)var4);
    org.junit.rules.ExpectedException var7 = var4.handleAssumptionViolatedExceptions();
    org.junit.rules.RuleChain var8 = var1.around((org.junit.rules.TestRule)var4);
    org.junit.rules.TemporaryFolder var9 = new org.junit.rules.TemporaryFolder();
    junit.framework.TestResult var10 = new junit.framework.TestResult();
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    junit.framework.TestSuite var16 = new junit.framework.TestSuite(var13);
    junit.framework.AssertionFailedError var18 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var22 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var23 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var22);
    org.junit.experimental.categories.Categories.CategoryFilter var24 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var22);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var25 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var18, "", (java.lang.Object[])var22);
    java.lang.String var26 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var25);
    var10.addError((junit.framework.Test)var16, (java.lang.Throwable)var25);
    org.junit.runners.model.InitializationError var28 = new org.junit.runners.model.InitializationError((java.lang.Throwable)var25);
    org.junit.internal.runners.statements.Fail var29 = new org.junit.internal.runners.statements.Fail((java.lang.Throwable)var25);
    org.junit.runner.Description var33 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var37 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var33.addChild(var37);
    org.junit.runners.model.Statement var39 = var9.apply((org.junit.runners.model.Statement)var29, var37);
    java.lang.annotation.Annotation[] var41 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var42 = org.junit.runner.Description.createSuiteDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", var41);
    org.junit.runners.model.Statement var43 = var4.apply((org.junit.runners.model.Statement)var29, var42);
    org.junit.runner.Description var47 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var51 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var47.addChild(var51);
    java.util.ArrayList var53 = var51.getChildren();
    org.junit.runners.model.MultipleFailureException var54 = new org.junit.runners.model.MultipleFailureException((java.util.List)var53);
    org.junit.internal.runners.model.MultipleFailureException var55 = new org.junit.internal.runners.model.MultipleFailureException((java.util.List)var53);
    org.junit.runners.model.MultipleFailureException var56 = new org.junit.runners.model.MultipleFailureException((java.util.List)var53);
    org.junit.rules.TemporaryFolder var57 = new org.junit.rules.TemporaryFolder();
    junit.framework.TestResult var58 = new junit.framework.TestResult();
    java.lang.Class[] var61 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var62 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var61);
    org.junit.experimental.categories.Categories.CategoryFilter var63 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var61);
    junit.framework.TestSuite var64 = new junit.framework.TestSuite(var61);
    junit.framework.AssertionFailedError var66 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var70 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var71 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var70);
    org.junit.experimental.categories.Categories.CategoryFilter var72 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var70);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var73 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var66, "", (java.lang.Object[])var70);
    java.lang.String var74 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var73);
    var58.addError((junit.framework.Test)var64, (java.lang.Throwable)var73);
    org.junit.runners.model.InitializationError var76 = new org.junit.runners.model.InitializationError((java.lang.Throwable)var73);
    org.junit.internal.runners.statements.Fail var77 = new org.junit.internal.runners.statements.Fail((java.lang.Throwable)var73);
    org.junit.runner.Description var81 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var85 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var81.addChild(var85);
    org.junit.runners.model.Statement var87 = var57.apply((org.junit.runners.model.Statement)var77, var85);
    org.junit.rules.RunRules var88 = new org.junit.rules.RunRules(var43, (java.lang.Iterable)var53, var85);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var26.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var51);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var61);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var62);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var63);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var70);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var71);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var72);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var74 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var74.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var81);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var85);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var87);

  }

  public void test156() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test156");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    junit.framework.TestResult var1 = new junit.framework.TestResult();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.TestSuite var7 = new junit.framework.TestSuite(var4);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    java.lang.String var17 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var16);
    var1.addError((junit.framework.Test)var7, (java.lang.Throwable)var16);
    java.lang.String var19 = var7.getName();
    var0.endTest((junit.framework.Test)var7);
    junit.framework.TestResult var21 = new junit.framework.TestResult();
    java.lang.Class[] var24 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var25 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var24);
    org.junit.experimental.categories.Categories.CategoryFilter var26 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var24);
    junit.framework.TestSuite var27 = new junit.framework.TestSuite(var24);
    junit.framework.AssertionFailedError var29 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var33 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var34 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var33);
    org.junit.experimental.categories.Categories.CategoryFilter var35 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var33);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var36 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var29, "", (java.lang.Object[])var33);
    java.lang.String var37 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var36);
    var21.addError((junit.framework.Test)var27, (java.lang.Throwable)var36);
    junit.extensions.RepeatedTest var40 = new junit.extensions.RepeatedTest((junit.framework.Test)var27, 1);
    junit.framework.TestResult var41 = new junit.framework.TestResult();
    java.lang.Class[] var44 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var45 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var44);
    org.junit.experimental.categories.Categories.CategoryFilter var46 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var44);
    junit.framework.TestSuite var47 = new junit.framework.TestSuite(var44);
    junit.framework.AssertionFailedError var49 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var53 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var54 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var53);
    org.junit.experimental.categories.Categories.CategoryFilter var55 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var53);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var56 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var49, "", (java.lang.Object[])var53);
    java.lang.String var57 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var56);
    var41.addError((junit.framework.Test)var47, (java.lang.Throwable)var56);
    var7.runTest((junit.framework.Test)var40, var41);
    java.util.Enumeration var60 = var7.tests();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var17.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var37.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var57.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var60);

  }

  public void test157() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test157");


    org.junit.runner.Description var4 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var8 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var4.addChild(var8);
    java.util.ArrayList var10 = var8.getChildren();
    org.junit.runners.model.MultipleFailureException var11 = new org.junit.runners.model.MultipleFailureException((java.util.List)var10);
    org.junit.runners.model.MultipleFailureException var12 = new org.junit.runners.model.MultipleFailureException((java.util.List)var10);
    float[] var14 = new float[] { };
    float[] var15 = new float[] { };
    org.junit.Assert.assertArrayEquals("", var14, var15, 1.0f);
    float[] var19 = new float[] { };
    float[] var20 = new float[] { };
    org.junit.Assert.assertArrayEquals("", var19, var20, 1.0f);
    org.junit.Assert.assertArrayEquals(var15, var20, 1.0f);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.failNotEquals("()", (java.lang.Object)var10, (java.lang.Object)var15);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);

  }

  public void test158() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test158");


    org.junit.rules.ExpectedException var0 = org.junit.rules.ExpectedException.none();
    org.junit.rules.ExpectedException var1 = var0.handleAssumptionViolatedExceptions();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);

  }

  public void test159() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test159");


    org.junit.runner.notification.RunNotifier var0 = new org.junit.runner.notification.RunNotifier();

  }

  public void test160() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test160");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    org.junit.runner.Result var6 = org.junit.runner.JUnitCore.runClasses(var3);
    org.junit.runner.Result var7 = org.junit.runner.JUnitCore.runClasses(var3);
    org.junit.runner.Computer var8 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var9 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var11 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var12 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var11);
    org.junit.runner.Runner var13 = var8.getSuite((org.junit.runners.model.RunnerBuilder)var9, var11);
    org.junit.Assert.assertArrayEquals((java.lang.Object[])var3, (java.lang.Object[])var11);
    org.hamcrest.Matcher var15 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.Assume.assumeThat((java.lang.Object)var11, var15);
      fail("Expected exception of type org.junit.AssumptionViolatedException");
    } catch (org.junit.AssumptionViolatedException e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);

  }

  public void test161() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test161");


    org.junit.internal.builders.JUnit4Builder var0 = new org.junit.internal.builders.JUnit4Builder();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.AssertionFailedError var8 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var12 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var12);
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var12);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var15 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var8, "", (java.lang.Object[])var12);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var4, (java.lang.Object[])var12);
    java.lang.Class[] var20 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var21 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var20);
    org.junit.experimental.categories.Categories.CategoryFilter var22 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var20);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var20);
    org.junit.experimental.categories.Categories.CategoryFilter var24 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var20);
    org.junit.Assert.assertEquals((java.lang.Object[])var12, (java.lang.Object[])var20);
    org.junit.runners.Suite var26 = new org.junit.runners.Suite((org.junit.runners.model.RunnerBuilder)var0, var20);
    java.lang.Class[] var30 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var31 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var30);
    org.junit.experimental.categories.Categories.CategoryFilter var32 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var30);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var30);
    org.junit.experimental.categories.Categories.CategoryFilter var34 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var30);
    java.lang.annotation.Annotation[] var37 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var38 = org.junit.runner.Description.createTestDescription("hi!", "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", var37);
    boolean var39 = var34.shouldRun(var38);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      var26.filter((org.junit.runner.manipulation.Filter)var34);
      fail("Expected exception of type org.junit.runner.manipulation.NoTestsRemainException");
    } catch (org.junit.runner.manipulation.NoTestsRemainException e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == true);

  }

  public void test162() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test162");


    org.junit.rules.ExpectedException var0 = org.junit.rules.ExpectedException.none();
    org.junit.rules.RuleChain var1 = org.junit.rules.RuleChain.outerRule((org.junit.rules.TestRule)var0);
    org.junit.rules.ExpectedException var2 = org.junit.rules.ExpectedException.none();
    org.junit.rules.RuleChain var3 = org.junit.rules.RuleChain.outerRule((org.junit.rules.TestRule)var2);
    org.junit.rules.RuleChain var4 = var1.around((org.junit.rules.TestRule)var2);
    org.junit.rules.ExpectedException var6 = var2.reportMissingExceptionWithMessage("");
    org.hamcrest.Matcher var7 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    org.junit.internal.matchers.ThrowableMessageMatcher var8 = new org.junit.internal.matchers.ThrowableMessageMatcher(var7);
    org.hamcrest.Matcher var9 = org.junit.matchers.JUnitMatchers.<java.lang.Exception>isException(var7);
    var6.expectMessage(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);

  }

  public void test163() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test163");


    junit.framework.AssertionFailedError var1 = new junit.framework.AssertionFailedError("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>");

  }

  public void test164() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test164");


    junit.framework.TestResult var1 = new junit.framework.TestResult();
    int var2 = var1.runCount();
    junit.framework.TestResult var3 = new junit.framework.TestResult();
    junit.framework.TestResult var4 = new junit.framework.TestResult();
    java.lang.Class[] var7 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var7);
    org.junit.experimental.categories.Categories.CategoryFilter var9 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var7);
    junit.framework.TestSuite var10 = new junit.framework.TestSuite(var7);
    junit.framework.AssertionFailedError var12 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var16 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var16);
    org.junit.experimental.categories.Categories.CategoryFilter var18 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var16);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var19 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var12, "", (java.lang.Object[])var16);
    java.lang.String var20 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var19);
    var4.addError((junit.framework.Test)var10, (java.lang.Throwable)var19);
    java.lang.String var22 = var10.getName();
    var3.endTest((junit.framework.Test)var10);
    var1.endTest((junit.framework.Test)var10);
    int var25 = var1.runCount();
    junit.framework.Assert.assertNotNull("()", (java.lang.Object)var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var20.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == 0);

  }

  public void test165() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test165");


    org.junit.runner.notification.RunListener var0 = new org.junit.runner.notification.RunListener();
    org.junit.rules.TemporaryFolder var1 = new org.junit.rules.TemporaryFolder();
    junit.framework.TestResult var2 = new junit.framework.TestResult();
    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var5);
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var5);
    junit.framework.TestSuite var8 = new junit.framework.TestSuite(var5);
    junit.framework.AssertionFailedError var10 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var14 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var14);
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var14);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var17 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var10, "", (java.lang.Object[])var14);
    java.lang.String var18 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var17);
    var2.addError((junit.framework.Test)var8, (java.lang.Throwable)var17);
    org.junit.runners.model.InitializationError var20 = new org.junit.runners.model.InitializationError((java.lang.Throwable)var17);
    org.junit.internal.runners.statements.Fail var21 = new org.junit.internal.runners.statements.Fail((java.lang.Throwable)var17);
    org.junit.runner.Description var25 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var29 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var25.addChild(var29);
    org.junit.runners.model.Statement var31 = var1.apply((org.junit.runners.model.Statement)var21, var29);
    org.junit.runner.Description var35 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var39 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var35.addChild(var39);
    java.util.ArrayList var41 = var39.getChildren();
    org.junit.runners.model.MultipleFailureException var42 = new org.junit.runners.model.MultipleFailureException((java.util.List)var41);
    org.junit.internal.runners.model.MultipleFailureException var43 = new org.junit.internal.runners.model.MultipleFailureException((java.util.List)var41);
    org.junit.runner.Result var44 = new org.junit.runner.Result();
    int var45 = var44.getIgnoreCount();
    int var46 = var44.getFailureCount();
    org.junit.internal.runners.statements.RunBefores var47 = new org.junit.internal.runners.statements.RunBefores((org.junit.runners.model.Statement)var21, (java.util.List)var41, (java.lang.Object)var44);
    var0.testRunFinished(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var18.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == 0);

  }

  public void test166() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test166");


    org.junit.internal.builders.JUnit4Builder var1 = new org.junit.internal.builders.JUnit4Builder();
    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var5);
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var5);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var5, (java.lang.Object[])var13);
    java.lang.Class[] var21 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var22 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var21);
    org.junit.experimental.categories.Categories.CategoryFilter var23 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var21);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var21);
    org.junit.experimental.categories.Categories.CategoryFilter var25 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var21);
    org.junit.Assert.assertEquals((java.lang.Object[])var13, (java.lang.Object[])var21);
    org.junit.runners.Suite var27 = new org.junit.runners.Suite((org.junit.runners.model.RunnerBuilder)var1, var21);
    junit.framework.JUnit4TestAdapterCache var28 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var29 = var28.toString();
    java.lang.Class[] var31 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var32 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var31);
    boolean var33 = var28.containsValue((java.lang.Object)var31);
    org.junit.runner.Result var34 = org.junit.runner.JUnitCore.runClasses(var31);
    org.junit.runner.Result var35 = org.junit.runner.JUnitCore.runClasses(var31);
    org.junit.runner.Computer var36 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var37 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var39 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var40 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var39);
    org.junit.runner.Runner var41 = var36.getSuite((org.junit.runners.model.RunnerBuilder)var37, var39);
    org.junit.Assert.assertArrayEquals((java.lang.Object[])var31, (java.lang.Object[])var39);
    org.junit.Assert.assertEquals("{}", (java.lang.Object[])var21, (java.lang.Object[])var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "{}"+ "'", var29.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);

  }

  public void test167() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test167");


    junit.framework.ComparisonCompactor var3 = new junit.framework.ComparisonCompactor(100, "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@1f489d9> but was:<100>", "");
    java.lang.String var5 = var3.compact("{}");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "{} expected:<[org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@1f489d9> but was:<100>]> but was:<[]>"+ "'", var5.equals("{} expected:<[org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@1f489d9> but was:<100>]> but was:<[]>"));

  }

  public void test168() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test168");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    org.junit.runner.Result var6 = org.junit.runner.JUnitCore.runClasses(var3);
    org.junit.runner.Result var7 = org.junit.runner.JUnitCore.runClasses(var3);
    org.junit.runner.Computer var8 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var9 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var11 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var12 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var11);
    org.junit.runner.Runner var13 = var8.getSuite((org.junit.runners.model.RunnerBuilder)var9, var11);
    org.junit.Assert.assertArrayEquals((java.lang.Object[])var3, (java.lang.Object[])var11);
    org.junit.runner.Result var15 = org.junit.runner.JUnitCore.runClasses(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);

  }

  public void test169() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test169");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    org.junit.runner.Computer var2 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var3 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var5);
    org.junit.runner.Runner var7 = var2.getSuite((org.junit.runners.model.RunnerBuilder)var3, var5);
    org.junit.runner.Computer var8 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var9 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var11 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var12 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var11);
    org.junit.runner.Runner var13 = var8.getSuite((org.junit.runners.model.RunnerBuilder)var9, var11);
    org.junit.internal.builders.IgnoredBuilder var14 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var18 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var19 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var18);
    org.junit.experimental.categories.Categories.CategoryFilter var20 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var18);
    junit.framework.AssertionFailedError var22 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var26 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var27 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var26);
    org.junit.experimental.categories.Categories.CategoryFilter var28 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var26);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var29 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var22, "", (java.lang.Object[])var26);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var18, (java.lang.Object[])var26);
    java.lang.Class[] var34 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var35 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var34);
    org.junit.experimental.categories.Categories.CategoryFilter var36 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var34);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var34);
    org.junit.experimental.categories.Categories.CategoryFilter var38 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var34);
    org.junit.Assert.assertEquals((java.lang.Object[])var26, (java.lang.Object[])var34);
    org.junit.runner.Runner var40 = var8.getSuite((org.junit.runners.model.RunnerBuilder)var14, var26);
    org.junit.runner.Result var41 = org.junit.runner.JUnitCore.runClasses(var2, var26);
    java.lang.Object var42 = var0.remove((java.lang.Object)var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);

  }

  public void test170() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test170");


    org.hamcrest.Matcher var0 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    org.hamcrest.Matcher var1 = org.junit.matchers.JUnitMatchers.hasItem(var0);
    org.hamcrest.Matcher var2 = org.junit.internal.matchers.ThrowableCauseMatcher.<java.lang.Throwable>hasCause(var0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);

  }

  public void test171() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test171");


    org.junit.internal.RealSystem var1 = new org.junit.internal.RealSystem();
    java.io.PrintStream var2 = var1.out();
    junit.textui.ResultPrinter var3 = new junit.textui.ResultPrinter(var2);
    junit.textui.TestRunner var4 = new junit.textui.TestRunner(var2);
    org.junit.runner.Computer var5 = new org.junit.runner.Computer();
    java.lang.Class[] var9 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var10 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var9);
    org.junit.experimental.categories.Categories.CategoryFilter var11 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var9);
    junit.framework.AssertionFailedError var13 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var17 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var18 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var17);
    org.junit.experimental.categories.Categories.CategoryFilter var19 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var17);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var20 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var13, "", (java.lang.Object[])var17);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var9, (java.lang.Object[])var17);
    java.lang.Class[] var25 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var26 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var25);
    org.junit.experimental.categories.Categories.CategoryFilter var27 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var25);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var25);
    org.junit.experimental.categories.Categories.CategoryFilter var29 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var25);
    org.junit.Assert.assertEquals((java.lang.Object[])var17, (java.lang.Object[])var25);
    org.junit.runner.Request var31 = org.junit.runner.Request.classes(var5, var25);
    java.lang.annotation.Annotation[] var33 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var34 = org.junit.runner.Description.createSuiteDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>", var33);
    org.junit.runner.Request var35 = var31.filterWith(var34);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("{} expected:<[org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@1f489d9> but was:<100>]> but was:<[]>", (java.lang.Object)var2, (java.lang.Object)var35);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);

  }

  public void test172() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test172");


    junit.textui.TestRunner var0 = new junit.textui.TestRunner();
    org.junit.internal.RealSystem var1 = new org.junit.internal.RealSystem();
    java.io.PrintStream var2 = var1.out();
    junit.textui.ResultPrinter var3 = new junit.textui.ResultPrinter(var2);
    var0.setPrinter(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);

  }

  public void test173() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test173");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    junit.framework.TestResult var1 = new junit.framework.TestResult();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.TestSuite var7 = new junit.framework.TestSuite(var4);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    java.lang.String var17 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var16);
    var1.addError((junit.framework.Test)var7, (java.lang.Throwable)var16);
    java.lang.String var19 = var7.getName();
    var0.endTest((junit.framework.Test)var7);
    java.util.Enumeration var21 = var7.tests();
    junit.framework.TestResult var22 = junit.textui.TestRunner.run((junit.framework.Test)var7);
    int var23 = var7.testCount();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var17.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == 0);

  }

  public void test174() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test174");


    org.junit.runner.Description var3 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var7 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var3.addChild(var7);
    java.util.ArrayList var9 = var7.getChildren();
    org.junit.runners.model.InitializationError var10 = new org.junit.runners.model.InitializationError((java.util.List)var9);
    org.junit.internal.runners.InitializationError var11 = new org.junit.internal.runners.InitializationError((java.util.List)var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);

  }

  public void test175() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test175");


    junit.textui.TestRunner var0 = new junit.textui.TestRunner();
    java.lang.String var2 = var0.extractClassName("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n"+ "'", var2.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n"));

  }

  public void test176() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test176");


    org.junit.internal.builders.SuiteMethodBuilder var1 = new org.junit.internal.builders.SuiteMethodBuilder();
    org.junit.runner.Computer var2 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var3 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var5);
    org.junit.runner.Runner var7 = var2.getSuite((org.junit.runners.model.RunnerBuilder)var3, var5);
    org.junit.runner.Computer var8 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var9 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var11 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var12 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var11);
    org.junit.runner.Runner var13 = var8.getSuite((org.junit.runners.model.RunnerBuilder)var9, var11);
    org.junit.internal.builders.IgnoredBuilder var14 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var18 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var19 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var18);
    org.junit.experimental.categories.Categories.CategoryFilter var20 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var18);
    junit.framework.AssertionFailedError var22 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var26 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var27 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var26);
    org.junit.experimental.categories.Categories.CategoryFilter var28 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var26);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var29 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var22, "", (java.lang.Object[])var26);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var18, (java.lang.Object[])var26);
    java.lang.Class[] var34 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var35 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var34);
    org.junit.experimental.categories.Categories.CategoryFilter var36 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var34);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var34);
    org.junit.experimental.categories.Categories.CategoryFilter var38 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var34);
    org.junit.Assert.assertEquals((java.lang.Object[])var26, (java.lang.Object[])var34);
    org.junit.runner.Runner var40 = var8.getSuite((org.junit.runners.model.RunnerBuilder)var14, var26);
    org.junit.runner.Result var41 = org.junit.runner.JUnitCore.runClasses(var2, var26);
    org.junit.runners.Suite var42 = new org.junit.runners.Suite((org.junit.runners.model.RunnerBuilder)var1, var26);
    junit.framework.JUnit4TestAdapterCache var43 = new junit.framework.JUnit4TestAdapterCache();
    boolean var45 = var43.containsKey((java.lang.Object)false);
    java.lang.Object var46 = var43.clone();
    junit.framework.Assert.assertNotSame("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", (java.lang.Object)var42, (java.lang.Object)var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var46);

  }

  public void test177() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test177");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    junit.framework.TestResult var1 = new junit.framework.TestResult();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.TestSuite var7 = new junit.framework.TestSuite(var4);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    java.lang.String var17 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var16);
    var1.addError((junit.framework.Test)var7, (java.lang.Throwable)var16);
    java.lang.String var19 = var7.getName();
    var0.endTest((junit.framework.Test)var7);
    junit.framework.TestResult var21 = new junit.framework.TestResult();
    java.lang.Class[] var24 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var25 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var24);
    org.junit.experimental.categories.Categories.CategoryFilter var26 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var24);
    junit.framework.TestSuite var27 = new junit.framework.TestSuite(var24);
    junit.framework.AssertionFailedError var29 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var33 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var34 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var33);
    org.junit.experimental.categories.Categories.CategoryFilter var35 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var33);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var36 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var29, "", (java.lang.Object[])var33);
    java.lang.String var37 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var36);
    var21.addError((junit.framework.Test)var27, (java.lang.Throwable)var36);
    junit.extensions.RepeatedTest var40 = new junit.extensions.RepeatedTest((junit.framework.Test)var27, 1);
    junit.framework.TestResult var41 = new junit.framework.TestResult();
    java.lang.Class[] var44 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var45 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var44);
    org.junit.experimental.categories.Categories.CategoryFilter var46 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var44);
    junit.framework.TestSuite var47 = new junit.framework.TestSuite(var44);
    junit.framework.AssertionFailedError var49 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var53 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var54 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var53);
    org.junit.experimental.categories.Categories.CategoryFilter var55 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var53);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var56 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var49, "", (java.lang.Object[])var53);
    java.lang.String var57 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var56);
    var41.addError((junit.framework.Test)var47, (java.lang.Throwable)var56);
    var7.runTest((junit.framework.Test)var40, var41);
    junit.framework.AssertionFailedError var61 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var65 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var66 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var65);
    org.junit.experimental.categories.Categories.CategoryFilter var67 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var65);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var68 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var61, "", (java.lang.Object[])var65);
    junit.framework.TestFailure var69 = new junit.framework.TestFailure((junit.framework.Test)var7, (java.lang.Throwable)var68);
    java.lang.String var70 = var69.exceptionMessage();
    java.lang.String var71 = var69.trace();
    java.lang.String var72 = var69.exceptionMessage();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var17.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var37.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var57.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var65);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var66);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var67);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var70 + "' != '" + "()"+ "'", var70.equals("()"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var71 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var71.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var72 + "' != '" + "()"+ "'", var72.equals("()"));

  }

  public void test178() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test178");


    org.junit.runner.Result var0 = new org.junit.runner.Result();
    int var1 = var0.getIgnoreCount();
    int var2 = var0.getIgnoreCount();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var1 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == 0);

  }

  public void test179() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test179");


    junit.framework.JUnit4TestAdapterCache var1 = new junit.framework.JUnit4TestAdapterCache();
    boolean var3 = var1.containsKey((java.lang.Object)false);
    int var4 = var1.size();
    java.lang.Class[] var7 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var7);
    org.junit.experimental.categories.Categories.CategoryFilter var9 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var7);
    java.lang.String var10 = junit.framework.Assert.format("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object)var1, (java.lang.Object)true);
    org.junit.runner.Description var14 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var18 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var14.addChild(var18);
    java.util.ArrayList var20 = var18.getChildren();
    org.junit.runners.model.MultipleFailureException var21 = new org.junit.runners.model.MultipleFailureException((java.util.List)var20);
    org.junit.internal.runners.model.MultipleFailureException var22 = new org.junit.internal.runners.model.MultipleFailureException((java.util.List)var20);
    org.hamcrest.Matcher var23 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    org.junit.internal.matchers.ThrowableMessageMatcher var24 = new org.junit.internal.matchers.ThrowableMessageMatcher(var23);
    java.lang.Object var25 = var1.put((java.lang.Object)var22, (java.lang.Object)var24);
    java.util.Collection var26 = var1.values();
    junit.framework.JUnit4TestAdapterCache var27 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var28 = var27.toString();
    java.lang.Class[] var30 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var31 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var30);
    boolean var32 = var27.containsValue((java.lang.Object)var30);
    junit.framework.JUnit4TestAdapterCache var33 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.Object var34 = var33.clone();
    var27.putAll((java.util.Map)var33);
    org.junit.runner.Description var39 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var43 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var39.addChild(var43);
    java.util.ArrayList var45 = var43.getChildren();
    java.lang.Object var46 = var27.get((java.lang.Object)var43);
    java.util.List var47 = var1.asTestList(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>"+ "'", var10.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "{}"+ "'", var28.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var47);

  }

  public void test180() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test180");


    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.AssertionFailedError var7 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var11 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var12 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var11);
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var11);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var14 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var7, "", (java.lang.Object[])var11);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var3, (java.lang.Object[])var11);
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(var11);
    java.lang.String var17 = var16.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "categories [all]"+ "'", var17.equals("categories [all]"));

  }

  public void test181() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test181");


    org.junit.internal.RealSystem var0 = new org.junit.internal.RealSystem();
    java.io.PrintStream var1 = var0.out();
    junit.textui.ResultPrinter var2 = new junit.textui.ResultPrinter(var1);
    junit.framework.TestResult var3 = new junit.framework.TestResult();
    java.lang.Class[] var6 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var6);
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var6);
    junit.framework.TestSuite var9 = new junit.framework.TestSuite(var6);
    junit.framework.AssertionFailedError var11 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var15 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var15);
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var15);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var18 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var11, "", (java.lang.Object[])var15);
    java.lang.String var19 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var18);
    var3.addError((junit.framework.Test)var9, (java.lang.Throwable)var18);
    var2.startTest((junit.framework.Test)var9);
    junit.textui.TestRunner var22 = new junit.textui.TestRunner(var2);
    java.lang.Class[] var25 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var26 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var25);
    org.junit.experimental.categories.Categories.CategoryFilter var27 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var25);
    junit.framework.TestSuite var29 = new junit.framework.TestSuite(var25, "");
    var2.startTest((junit.framework.Test)var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var19.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);

  }

  public void test182() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test182");


    org.junit.Assert.assertTrue("4.13-SNAPSHOT", true);

  }

  public void test183() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test183");


    junit.runner.BaseTestRunner.setPreference("hi!", "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n");

  }

  public void test184() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test184");


    org.junit.Assert.assertEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unk...", 10L, 10L);

  }

  public void test185() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test185");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.runners.MethodSorters var1 = org.junit.runners.MethodSorters.valueOf("{}");
      fail("Expected exception of type java.lang.IllegalArgumentException");
    } catch (java.lang.IllegalArgumentException e) {
      // Expected exception.
    }

  }

  public void test186() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test186");


    org.junit.internal.RealSystem var0 = new org.junit.internal.RealSystem();
    java.io.PrintStream var1 = var0.out();
    java.io.PrintStream var2 = var0.out();
    java.io.PrintStream var3 = var0.out();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);

  }

  public void test187() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test187");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    junit.framework.TestResult var1 = new junit.framework.TestResult();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.TestSuite var7 = new junit.framework.TestSuite(var4);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    java.lang.String var17 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var16);
    var1.addError((junit.framework.Test)var7, (java.lang.Throwable)var16);
    java.lang.String var19 = var7.getName();
    var0.endTest((junit.framework.Test)var7);
    java.util.Enumeration var21 = var7.tests();
    junit.framework.TestResult var22 = junit.textui.TestRunner.run((junit.framework.Test)var7);
    java.lang.String var23 = var7.getName();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var17.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);

  }

  public void test188() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test188");


    org.junit.internal.runners.InitializationError var1 = new org.junit.internal.runners.InitializationError("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>");
    java.util.List var2 = var1.getCauses();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);

  }

  public void test189() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test189");


    junit.framework.Assert.assertEquals("hi!", (byte)(-1), (byte)(-1));

  }

  public void test190() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test190");


    float[] var3 = new float[] { };
    float[] var4 = new float[] { };
    org.junit.Assert.assertArrayEquals("", var3, var4, 1.0f);
    float[] var9 = new float[] { };
    float[] var10 = new float[] { };
    org.junit.Assert.assertArrayEquals("", var9, var10, 1.0f);
    float[] var14 = new float[] { };
    float[] var15 = new float[] { };
    org.junit.Assert.assertArrayEquals("", var14, var15, 1.0f);
    org.junit.Assert.assertArrayEquals(var10, var15, 1.0f);
    float[] var21 = new float[] { };
    float[] var22 = new float[] { };
    org.junit.Assert.assertArrayEquals("", var21, var22, 1.0f);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", var15, var21, (-1.0f));
    org.junit.Assert.assertArrayEquals("", var4, var21, 0.0f);
    org.junit.runner.Computer var30 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var31 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var33 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var34 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var33);
    org.junit.runner.Runner var35 = var30.getSuite((org.junit.runners.model.RunnerBuilder)var31, var33);
    org.junit.internal.builders.IgnoredBuilder var36 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var40 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var41 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var40);
    org.junit.experimental.categories.Categories.CategoryFilter var42 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var40);
    junit.framework.AssertionFailedError var44 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var48 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var49 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var48);
    org.junit.experimental.categories.Categories.CategoryFilter var50 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var48);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var51 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var44, "", (java.lang.Object[])var48);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var40, (java.lang.Object[])var48);
    java.lang.Class[] var56 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var57 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var56);
    org.junit.experimental.categories.Categories.CategoryFilter var58 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var56);
    org.junit.Assume.assumeNotNull((java.lang.Object[])var56);
    org.junit.experimental.categories.Categories.CategoryFilter var60 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var56);
    org.junit.Assert.assertEquals((java.lang.Object[])var48, (java.lang.Object[])var56);
    org.junit.runner.Runner var62 = var30.getSuite((org.junit.runners.model.RunnerBuilder)var36, var48);
    java.lang.String var64 = junit.framework.Assert.format("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", (java.lang.Object)var30, (java.lang.Object)100L);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      org.junit.Assert.assertEquals("categories [all]", (java.lang.Object)"", (java.lang.Object)var64);
      fail("Expected exception of type org.junit.ComparisonFailure");
    } catch (org.junit.ComparisonFailure e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var48);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var57);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var58);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var60);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var62);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var64 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@13e6657> but was:<100>"+ "'", var64.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n expected:<org.junit.runner.Computer@13e6657> but was:<100>"));

  }

  public void test191() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test191");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", 1, 0);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test192() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test192");


    java.lang.annotation.Annotation[] var1 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var2 = org.junit.runner.Description.createSuiteDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>", var1);
    java.util.ArrayList var3 = var2.getChildren();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);

  }

  public void test193() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test193");


    org.junit.rules.TemporaryFolder var0 = new org.junit.rules.TemporaryFolder();
    junit.framework.TestResult var1 = new junit.framework.TestResult();
    java.lang.Class[] var4 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var4);
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var4);
    junit.framework.TestSuite var7 = new junit.framework.TestSuite(var4);
    junit.framework.AssertionFailedError var9 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var13 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var13);
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var13);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var16 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var9, "", (java.lang.Object[])var13);
    java.lang.String var17 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var16);
    var1.addError((junit.framework.Test)var7, (java.lang.Throwable)var16);
    org.junit.runners.model.InitializationError var19 = new org.junit.runners.model.InitializationError((java.lang.Throwable)var16);
    org.junit.internal.runners.statements.Fail var20 = new org.junit.internal.runners.statements.Fail((java.lang.Throwable)var16);
    org.junit.runner.Description var24 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var28 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var24.addChild(var28);
    org.junit.runners.model.Statement var30 = var0.apply((org.junit.runners.model.Statement)var20, var28);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      java.io.File var32 = var0.newFolder("categories [all]");
      fail("Expected exception of type java.lang.IllegalStateException");
    } catch (java.lang.IllegalStateException e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var17.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var30);

  }

  public void test194() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test194");


    org.junit.rules.TemporaryFolder var0 = new org.junit.rules.TemporaryFolder();
    java.lang.String[] var2 = new java.lang.String[] { "4.13-SNAPSHOT"};
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      java.io.File var3 = var0.newFolder(var2);
      fail("Expected exception of type java.lang.IllegalStateException");
    } catch (java.lang.IllegalStateException e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);

  }

  public void test195() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test195");


    org.junit.runner.Result var0 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var1 = var0.createListener();
    org.junit.runner.Computer var2 = new org.junit.runner.Computer();
    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var5);
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var5);
    junit.framework.TestSuite var8 = new junit.framework.TestSuite(var5);
    org.junit.runner.Result var9 = org.junit.runner.JUnitCore.runClasses(var2, var5);
    var1.testRunFinished(var9);
    boolean var11 = var9.wasSuccessful();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == true);

  }

  public void test196() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test196");


    org.junit.internal.RealSystem var0 = new org.junit.internal.RealSystem();
    java.io.PrintStream var1 = var0.out();
    junit.textui.ResultPrinter var2 = new junit.textui.ResultPrinter(var1);
    junit.textui.TestRunner var3 = new junit.textui.TestRunner(var1);
    org.junit.internal.TextListener var4 = new org.junit.internal.TextListener(var1);
    org.junit.internal.TextListener var5 = new org.junit.internal.TextListener(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);

  }

  public void test197() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test197");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.TestSuite var6 = new junit.framework.TestSuite(var3);
    junit.framework.AssertionFailedError var8 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var12 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var12);
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var12);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var15 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var8, "", (java.lang.Object[])var12);
    java.lang.String var16 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var15);
    var0.addError((junit.framework.Test)var6, (java.lang.Throwable)var15);
    java.util.Enumeration var18 = var6.tests();
    org.junit.runner.Computer var19 = new org.junit.runner.Computer();
    java.lang.Class[] var22 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var23 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var22);
    org.junit.experimental.categories.Categories.CategoryFilter var24 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var22);
    junit.framework.TestSuite var25 = new junit.framework.TestSuite(var22);
    org.junit.runner.Result var26 = org.junit.runner.JUnitCore.runClasses(var19, var22);
    org.junit.runner.Computer var27 = new org.junit.runner.Computer();
    org.junit.internal.builders.IgnoredBuilder var28 = new org.junit.internal.builders.IgnoredBuilder();
    java.lang.Class[] var30 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var31 = org.junit.experimental.categories.Categories.CategoryFilter.include(true, var30);
    org.junit.runner.Runner var32 = var27.getSuite((org.junit.runners.model.RunnerBuilder)var28, var30);
    org.junit.runner.Result var33 = org.junit.runner.JUnitCore.runClasses(var19, var30);
    org.junit.Assert.assertNotEquals((java.lang.Object)var6, (java.lang.Object)var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var16.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);

  }

  public void test198() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test198");


    org.junit.internal.RealSystem var0 = new org.junit.internal.RealSystem();
    java.io.PrintStream var1 = var0.out();
    junit.textui.ResultPrinter var2 = new junit.textui.ResultPrinter(var1);
    junit.textui.TestRunner var3 = new junit.textui.TestRunner(var1);
    org.junit.internal.TextListener var4 = new org.junit.internal.TextListener(var1);
    java.lang.annotation.Annotation[] var8 = new java.lang.annotation.Annotation[] { };
    org.junit.runner.Description var9 = org.junit.runner.Description.createSuiteDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>", var8);
    org.junit.runner.Description var10 = org.junit.runner.Description.createTestDescription("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n", var8);
    var4.testStarted(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var10);

  }

  public void test199() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test199");


    org.junit.experimental.categories.CategoryValidator var0 = new org.junit.experimental.categories.CategoryValidator();

  }

  public void test200() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test200");


    junit.textui.TestRunner var0 = new junit.textui.TestRunner();
    junit.framework.TestResult var1 = new junit.framework.TestResult();
    int var2 = var1.runCount();
    java.util.Enumeration var3 = var1.errors();
    junit.extensions.ActiveTestSuite var4 = new junit.extensions.ActiveTestSuite();
    junit.framework.AssertionFailedError var7 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var11 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var12 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var11);
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var11);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var14 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var7, "", (java.lang.Object[])var11);
    org.junit.AssumptionViolatedException var15 = new org.junit.AssumptionViolatedException("hi!", (java.lang.Throwable)var7);
    var1.addFailure((junit.framework.Test)var4, var7);
    junit.extensions.RepeatedTest var18 = new junit.extensions.RepeatedTest((junit.framework.Test)var4, 100);
    var0.endTest((junit.framework.Test)var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);

  }

  public void test201() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test201");


    junit.framework.JUnit4TestAdapterCache var1 = new junit.framework.JUnit4TestAdapterCache();
    boolean var3 = var1.containsKey((java.lang.Object)false);
    int var4 = var1.size();
    java.lang.Class[] var7 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var7);
    org.junit.experimental.categories.Categories.CategoryFilter var9 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var7);
    java.lang.String var10 = junit.framework.Assert.format("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object)var1, (java.lang.Object)true);
    org.junit.internal.builders.JUnit3Builder var11 = new org.junit.internal.builders.JUnit3Builder();
    boolean var12 = var1.equals((java.lang.Object)var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>"+ "'", var10.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n expected:<{}> but was:<true>"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);

  }

  public void test202() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test202");


    org.junit.internal.ExactComparisonCriteria var0 = new org.junit.internal.ExactComparisonCriteria();

  }

  public void test203() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test203");


    org.junit.rules.TemporaryFolder var0 = new org.junit.rules.TemporaryFolder();
    var0.create();
    org.junit.rules.DisableOnDebug var2 = new org.junit.rules.DisableOnDebug((org.junit.rules.TestRule)var0);

  }

  public void test204() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test204");


    junit.framework.JUnit4TestAdapterCache var0 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.String var1 = var0.toString();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    boolean var5 = var0.containsValue((java.lang.Object)var3);
    junit.framework.JUnit4TestAdapterCache var6 = new junit.framework.JUnit4TestAdapterCache();
    java.lang.Object var7 = var6.clone();
    var0.putAll((java.util.Map)var6);
    var6.clear();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "{}"+ "'", var1.equals("{}"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);

  }

  public void test205() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test205");


    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.framework.Assert.assertEquals((-1), 0);
      fail("Expected exception of type junit.framework.AssertionFailedError");
    } catch (junit.framework.AssertionFailedError e) {
      // Expected exception.
    }

  }

  public void test206() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test206");


    org.junit.rules.TestName var0 = new org.junit.rules.TestName();
    java.lang.String var1 = var0.getMethodName();
    junit.framework.TestResult var2 = new junit.framework.TestResult();
    java.lang.Class[] var5 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var6 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var5);
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var5);
    junit.framework.TestSuite var8 = new junit.framework.TestSuite(var5);
    junit.framework.AssertionFailedError var10 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var14 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var15 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var14);
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var14);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var17 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var10, "", (java.lang.Object[])var14);
    java.lang.String var18 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var17);
    var2.addError((junit.framework.Test)var8, (java.lang.Throwable)var17);
    org.junit.runners.model.InitializationError var20 = new org.junit.runners.model.InitializationError((java.lang.Throwable)var17);
    org.junit.internal.runners.statements.Fail var21 = new org.junit.internal.runners.statements.Fail((java.lang.Throwable)var17);
    org.junit.runner.Result var22 = new org.junit.runner.Result();
    org.junit.runner.notification.RunListener var23 = var22.createListener();
    org.junit.runner.Description var27 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    org.junit.runner.Description var31 = org.junit.runner.Description.createTestDescription("hi!", "hi!", (java.io.Serializable)"hi!");
    var27.addChild(var31);
    junit.framework.Assert.assertNotSame((java.lang.Object)var23, (java.lang.Object)var27);
    int var34 = var27.testCount();
    org.junit.runners.model.Statement var35 = var0.apply((org.junit.runners.model.Statement)var21, var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var18.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == 1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);

  }

  public void test207() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test207");


    junit.framework.AssertionFailedError var1 = new junit.framework.AssertionFailedError("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unk...");

  }

  public void test208() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test208");


    org.junit.runner.Result var0 = new org.junit.runner.Result();
    int var1 = var0.getRunCount();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var1 == 0);

  }

  public void test209() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test209");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    java.lang.Class[] var3 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var4 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var3);
    org.junit.experimental.categories.Categories.CategoryFilter var5 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var3);
    junit.framework.TestSuite var6 = new junit.framework.TestSuite(var3);
    junit.framework.AssertionFailedError var8 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var12 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var13 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var12);
    org.junit.experimental.categories.Categories.CategoryFilter var14 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var12);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var15 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var8, "", (java.lang.Object[])var12);
    java.lang.String var16 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var15);
    var0.addError((junit.framework.Test)var6, (java.lang.Throwable)var15);
    junit.extensions.RepeatedTest var19 = new junit.extensions.RepeatedTest((junit.framework.Test)var6, 1);
    junit.framework.TestResult var20 = new junit.framework.TestResult();
    junit.framework.TestResult var21 = new junit.framework.TestResult();
    java.lang.Class[] var24 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var25 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var24);
    org.junit.experimental.categories.Categories.CategoryFilter var26 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var24);
    junit.framework.TestSuite var27 = new junit.framework.TestSuite(var24);
    junit.framework.AssertionFailedError var29 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var33 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var34 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var33);
    org.junit.experimental.categories.Categories.CategoryFilter var35 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var33);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var36 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var29, "", (java.lang.Object[])var33);
    java.lang.String var37 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var36);
    var21.addError((junit.framework.Test)var27, (java.lang.Throwable)var36);
    java.lang.String var39 = var27.getName();
    var20.endTest((junit.framework.Test)var27);
    java.util.Enumeration var41 = var20.errors();
    var19.basicRun(var20);
    junit.extensions.ActiveTestSuite var43 = new junit.extensions.ActiveTestSuite();
    var20.startTest((junit.framework.Test)var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var16.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var37.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);

  }

  public void test210() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test210");


    org.junit.experimental.ParallelComputer var2 = new org.junit.experimental.ParallelComputer(true, false);
    org.junit.internal.builders.JUnit4Builder var3 = new org.junit.internal.builders.JUnit4Builder();
    java.lang.Class[] var7 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var7);
    org.junit.experimental.categories.Categories.CategoryFilter var9 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var7);
    junit.framework.AssertionFailedError var11 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var15 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var15);
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var15);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var18 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var11, "", (java.lang.Object[])var15);
    org.junit.Assert.assertArrayEquals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\t... 8 more\r\n", (java.lang.Object[])var7, (java.lang.Object[])var15);
    org.junit.experimental.categories.Categories.CategoryFilter var20 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(var15);
    org.junit.runner.Runner var21 = var2.getSuite((org.junit.runners.model.RunnerBuilder)var3, var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var21);

  }

  public void test211() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test211");


    org.hamcrest.Matcher var0 = org.junit.experimental.results.ResultMatchers.isSuccessful();
    org.junit.internal.matchers.ThrowableMessageMatcher var1 = new org.junit.internal.matchers.ThrowableMessageMatcher(var0);
    org.hamcrest.Matcher var2 = org.junit.internal.matchers.StacktracePrintingMatcher.<java.lang.Exception>isException((org.hamcrest.Matcher)var1);
    org.hamcrest.Matcher var3 = org.junit.matchers.JUnitMatchers.<java.lang.Exception>isException((org.hamcrest.Matcher)var1);
    org.junit.internal.matchers.ThrowableMessageMatcher var4 = new org.junit.internal.matchers.ThrowableMessageMatcher((org.hamcrest.Matcher)var1);
    org.hamcrest.Matcher var5 = org.junit.matchers.JUnitMatchers.hasItem((org.hamcrest.Matcher)var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);

  }

  public void test212() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest0.test212");


    junit.framework.TestResult var0 = new junit.framework.TestResult();
    int var1 = var0.runCount();
    java.util.Enumeration var2 = var0.failures();
    junit.framework.TestResult var3 = new junit.framework.TestResult();
    java.lang.Class[] var6 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var7 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var6);
    org.junit.experimental.categories.Categories.CategoryFilter var8 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var6);
    junit.framework.TestSuite var9 = new junit.framework.TestSuite(var6);
    junit.framework.AssertionFailedError var11 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var15 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var16 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var15);
    org.junit.experimental.categories.Categories.CategoryFilter var17 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var15);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var18 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var11, "", (java.lang.Object[])var15);
    java.lang.String var19 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var18);
    var3.addError((junit.framework.Test)var9, (java.lang.Throwable)var18);
    junit.extensions.RepeatedTest var22 = new junit.extensions.RepeatedTest((junit.framework.Test)var9, 1);
    var0.startTest((junit.framework.Test)var9);
    junit.extensions.ActiveTestSuite var24 = new junit.extensions.ActiveTestSuite();
    junit.extensions.ActiveTestSuite var25 = new junit.extensions.ActiveTestSuite();
    junit.framework.TestResult var26 = new junit.framework.TestResult();
    junit.framework.TestResult var27 = new junit.framework.TestResult();
    java.lang.Class[] var30 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var31 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var30);
    org.junit.experimental.categories.Categories.CategoryFilter var32 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var30);
    junit.framework.TestSuite var33 = new junit.framework.TestSuite(var30);
    junit.framework.AssertionFailedError var35 = new junit.framework.AssertionFailedError("hi!");
    java.lang.Class[] var39 = new java.lang.Class[] { };
    org.junit.experimental.categories.Categories.CategoryFilter var40 = org.junit.experimental.categories.Categories.CategoryFilter.include(false, var39);
    org.junit.experimental.categories.Categories.CategoryFilter var41 = org.junit.experimental.categories.Categories.CategoryFilter.exclude(true, var39);
    org.junit.experimental.theories.internal.ParameterizedAssertionError var42 = new org.junit.experimental.theories.internal.ParameterizedAssertionError((java.lang.Throwable)var35, "", (java.lang.Object[])var39);
    java.lang.String var43 = junit.runner.BaseTestRunner.getFilteredTrace((java.lang.Throwable)var42);
    var27.addError((junit.framework.Test)var33, (java.lang.Throwable)var42);
    java.lang.String var45 = var33.getName();
    var26.endTest((junit.framework.Test)var33);
    java.util.Enumeration var47 = var26.errors();
    var24.runTest((junit.framework.Test)var25, var26);
    var9.addTest((junit.framework.Test)var25);
    junit.framework.Test var51 = var9.testAt(0);
    // The following exception was thrown during execution.
    // This behavior will recorded for regression testing.
    try {
      junit.textui.TestRunner.runAndWait(var51);
      fail("Expected exception of type randoop.util.ReflectionExecutor.TimeoutExceeded");
    } catch (randoop.util.ReflectionExecutor.TimeoutExceeded e) {
      // Expected exception.
    }
    
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var1 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var19.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + "org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"+ "'", var43.equals("org.junit.experimental.theories.internal.ParameterizedAssertionError: ()\r\n\tat sun.reflect.GeneratedConstructorAccessor7.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat randoop.util.ConstructorReflectionCode.runReflectionCodeRaw(Unknown Source)\r\n\tat randoop.util.ReflectionCode.runReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.executeReflectionCode(Unknown Source)\r\n\tat randoop.util.RunnerThread.run(Unknown Source)\r\nCaused by: junit.framework.AssertionFailedError: hi!\r\n\tat sun.reflect.GeneratedConstructorAccessor6.newInstance(Unknown Source)\r\n\t... 6 more\r\n"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var51);

  }

}
