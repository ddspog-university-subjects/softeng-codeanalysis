package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_22 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_22.test1");


    float[] var1 = new float[] { };
    float[] var2 = new float[] { };
    org.junit.Assert.assertArrayEquals("", var1, var2, 1.0f);
    float[] var6 = new float[] { };
    float[] var7 = new float[] { };
    org.junit.Assert.assertArrayEquals("", var6, var7, 1.0f);
    org.junit.Assert.assertArrayEquals(var2, var7, 1.0f);
    float[] var14 = new float[] { 1.0f, 0.0f};
    org.junit.Assert.assertArrayEquals(var7, var14, 1.0f);

  }

}
