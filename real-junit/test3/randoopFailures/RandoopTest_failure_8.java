package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_8 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_8.test1");


    char[] var1 = new char[] { '4'};
    char[] var2 = new char[] { };
    org.junit.Assert.assertArrayEquals(var1, var2);

  }

}
