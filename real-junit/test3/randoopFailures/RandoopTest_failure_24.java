package randoopFailures;

import junit.framework.*;

public class RandoopTest_failure_24 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest_failure_24.test1");


    double[] var2 = new double[] { (-1.0d), 100.0d};
    double[] var6 = new double[] { 1.0d, 100.0d, 10.0d};
    org.junit.Assert.assertArrayEquals(var2, var6, 0.0d);

  }

}
